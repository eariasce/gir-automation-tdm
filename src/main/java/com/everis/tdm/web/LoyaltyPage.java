package com.everis.tdm.web;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://epvw-test.fa.us6.oraclecloud.com/")
public class LoyaltyPage extends PageObject {

    @FindBy(id = "userid")
    public WebElementFacade textoUsuario;

    @FindBy(id = "password")
    public WebElementFacade textoPassword;

    @FindBy(id = "btnActive")
    public WebElementFacade btnContinuar;

    @FindBy(id = "pt1:atkph1::_afrTtxt")
    public WebElementFacade textoBienvenida;

    @FindBy(id = "pt1:_UISpgl53u")
    public WebElementFacade menuHome;

    @FindBy(id = "pt1:_UISnvr:0:nvgpgl2_groupNode_Loyalty")
    public WebElementFacade menuFidelizacion;

    @FindBy(id = "pt1:_UISnvr:0:nv_itemNode_Loyalty_LoyaltyMembers")
    public WebElementFacade menuMiembros;

    @FindBy(id = "_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:ls1:slctChoice::content")
    public WebElementFacade comboLista;

    @FindBy(xpath = "//*[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:ls1:slctChoice::pop']/li[5]")
    public WebElementFacade comboTexto;

    @FindBy(xpath = "//*[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:ls1:lsq::saveSearch::drop']")
    public WebElementFacade comboHistoricoTC;

    @FindBy(xpath = "//*[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:ls1:lsq::saveSearch::pop']/li[3]")
    public WebElementFacade comboHistoricoTexto;

    @FindBy(xpath = "//*[@id=\"_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:ls1:lsq:addFields\"]")
    public WebElementFacade btnAgregarFiltro;

    @FindBy(xpath = "//*[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:ls1:lsq:addFieldsIter:31:adFldMenuItem']/td[2]")
    public WebElementFacade filtroTextoCodigoUnico;

    @FindBy(id = "_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:ls1:lsq:fldM::sDwnBg")
    public WebElementFacade downScroll;

    @FindBy(id = "_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:ls1:lsq:value40::content")
    public WebElementFacade textoCodigoUnico;

    @FindBy(id = "_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:ls1:lsq::search")
    public WebElementFacade botonBuscar;

    @FindBy(id = "_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:ls1:closeps")
    public WebElementFacade cerrarFiltro;

    @FindBy(xpath = "//*[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:ls1:call1:t1::db']/table/tbody/tr[1]/td[2]/span")
    public WebElementFacade numeroContrato;

    @FindBy(xpath = "//*[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:ls1:call1:t1::db']/table/tbody/tr[1]/td[1]/span")
    public WebElementFacade nombreMiembro;

    @FindBy(id = "_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:Profile::ti")
    public WebElementFacade menuPerfil;

    @FindBy(xpath = "//*[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:r1:1:r5:0:t1:0:pgl1']/tbody/tr/td[2]")
    public WebElementFacade puntosMillaPerfil;

    @FindBy(id = "_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:Transactions::ti")
    public WebElementFacade menuTransacciones;

    @FindBy(id = "_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:r4:0:stsSrch:create-transaction-commandButton")
    public WebElementFacade btnCrearTransaccion;

    @FindBy(id = "_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:r4:0:r1:0:SP1:soc5::drop")
    public WebElementFacade comboTipoTransaccion;

    @FindBy(xpath = "//*[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:r4:0:r1:0:SP1:soc5::pop']/li[3]")
    public WebElementFacade comboOpcionAcumular;

    @FindBy(xpath = "//*[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:r4:0:r1:0:SP1:soc7::drop']")
    public WebElementFacade comboSubTipoTransaccion;

    @FindBy(xpath = "//*[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:r4:0:r1:0:SP1:soc7::pop']/li[4]")
    public WebElementFacade comboOpcionSubTipoTransaccion;

    @FindBy(id = "_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:r4:0:r1:0:SP1:soc11::drop")
    public WebElementFacade comboTipoPuntos;

    @FindBy(xpath = "//*[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:r4:0:r1:0:SP1:soc11::pop']/li[3]")
    public WebElementFacade comboTipoPuntosMillas;

    @FindBy(id = "_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:r4:0:r1:0:SP1:it3::content")
    public WebElementFacade textoPuntos;

    @FindBy(id = "_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:r4:0:r1:0:SP1:ExtGen_attr_OCSConceptoDeOperacion_c_Transaction1::drop")
    public WebElementFacade comboConteptoOperacion;

    @FindBy(xpath = "//*[@id=\"_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:r4:0:r1:0:SP1:ExtGen_attr_OCSConceptoDeOperacion_c_Transaction1::pop\"]/li[11]")
    public WebElementFacade comboConceptoTexto;

    @FindBy(id = "_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:r4:0:r1:0:SP1:cb1")
    public WebElementFacade btnProcesar;

    @FindBy(id = "_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:r4:0:r1:0:SP1:d1::yes")
    public WebElementFacade btnProcesarYes;

    @FindBy(id = "_FOpt1:_FOr1:0:_FONSr2:0:_FOTsr1:0:object-subtitle:r2:0:object-subtitle:r4:0:r1:1:SP1:r1:0:soc20::content")
    public WebElementFacade transaccionEstado;


}
