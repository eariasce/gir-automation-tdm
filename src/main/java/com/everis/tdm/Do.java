package com.everis.tdm;

import com.everis.tdm.commons.UploadData;
import com.everis.tdm.config.AppConfig;
import com.everis.tdm.dao.giru.giruDAO;
import com.everis.tdm.model.giru.*;
import com.everis.tdm.services.AltaTarjeta;
import com.everis.tdm.services.ConsultarDeuda;
import com.everis.tdm.services.CrearCliente;
import com.everis.tdm.services.Token;
import com.everis.tdm.utils.QueryService;
import com.everis.tdm.web.LoyaltyStep;
import net.thucydides.core.annotations.Steps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.FileNotFoundException;
import java.util.stream.Collectors;
import java.util.List;

import static com.everis.tdm.security.Decryption.decrypt;
import static com.everis.tdm.security.Encryption.encrypt;


public class Do {

    private static final Logger LOGGER = LoggerFactory.getLogger(Do.class);
    static AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
    static giruDAO dao = context.getBean(giruDAO.class);
    @Steps
    static LoyaltyStep loyaltyStep;

    public static void spLeerDocumentosExcel(String data, String function) throws FileNotFoundException {
        UploadData.leerExcelDocumentos(data, function);
    }

    public static void uploadFullDataClient(String documentClient, String typeDocument) {
        ClientData clientData = QueryService.getClientData(documentClient, typeDocument);
        //Método que consulta la info del cliente y Guarda sus daots
        UploadData.uploadClientData(clientData);
        //Método que consulta la info del cliente y las cuentas usando APIs
        UploadData.uploadAccountData(clientData,typeDocument);
        //Método que consulta la info de las Tarjetas de crédito del cliente usando API
        UploadData.uploadCreditCardClient(clientData,typeDocument);
        //Método que consulta la info de las Tarjetas de débito del cliente usando API
        UploadData.uploadDebitCardClient(clientData,typeDocument);
    }

    public static void spIngresarDatosCliente(ClientData clientData) {
        if (Do.spSearchClient(clientData.getUniqueCode()).size() == 0) {
            dao.spIngresarDatosCliente( clientData);
            System.out.println("Se REGISTRÓ la información en la tabla ClientData");

        } else {
            dao.spUpdateClientData(clientData);
            System.out.println("El cliente ya existe, se ACTUALIZÓ la información en la tabla ClientData");
        }
    }

    public static void spDeleteAccountByAccountNumber(String uniqueCode) {
        dao.spDeleteAccountByAccountNumber(uniqueCode);
    }

    public static void spIngresarDatosCuenta(Account current, String uniqueCode) {
        dao.spSetAccount(
                uniqueCode,
                current.getType(),
                current.getProductCode(),
                current.getSubType(),
                current.getSubProductCode(),
                current.getCurrency(),
                current.getBalance(),
                current.getAvailableBalance(),
                current.getStatus(),
                current.getOffice(),
                current.getAccountNumber(),
                current.getJoint_type(),
                current.getCci(),
                current.getId());
    }

    public static void spDeleteCreditCardDataByCardNumber(String cardNumber) {
        dao.spDeleteCreditCardDataByCardNumber(cardNumber);
    }

    public static void spIngresarDatosTarjeta(AccountCreditCard accountCreditCard, String uniqueCode) {
        dao.spIngresarDatosTarjeta(
                uniqueCode,
                accountCreditCard.getAccountNumber(),
                accountCreditCard.getCurrency(),
                accountCreditCard.getCreditLine(),
                accountCreditCard.getCreditCard());

        System.out.println("Se INGRESÓ la tarjeta '" + accountCreditCard.getAccountNumber() + "' en la tabla CreditCard");
    }

    public static void spIngresarDatosTarjetaDebito(DebitCard debitCard) {
        if (!debitCard.getUniqueCode().isEmpty()) {
            dao.spSetDebitCardData(debitCard);
        } else {
            System.out.println("La tarjeta ingresada ya existe en la base de datos de TDM");
        }

    }

    public static void spDeleteDebitCardDataByCardNumber(String cardNumber) {
        dao.spDeleteDebitCardDataByCardNumber(cardNumber);
    }

    public static List<ClientCreditCard> spGetCreditCardForCheckMovement() {
        return dao.spGetCreditCardForCheckMovement();
    }

    public static List<ClientData> searchNumberClients(String documentClient) {
        return dao.spSearchNumberClients(documentClient);
    }

    public static List<ClientCreditCard> spGetCreditCardWithMovement(String movimiento) {
        return dao.spGetCreditCardWithMovement(movimiento);
    }

    public static List<ClientData2> spSearchClient(String uniqueCode) {
        return dao.spSearchClient(uniqueCode);
    }
    public static void spIngresarDatosClienteAddress(List<ClientAddress> clientAddress,String uniqueCode) {
        dao.spIngresarDatosClienteAddress(clientAddress, uniqueCode);
        System.out.println("Se REGISTRÓ la información en la tabla ClientAddress");

    }

    public static void spIngresarDatosClienteCompany(ClientCompany clientCompany) {
        dao.spIngresarDatosClienteCompany(clientCompany);
        System.out.println("Se REGISTRÓ la información en la tabla ClientCompany");
    }

    public static void spIngresarDatosClienteEmail(List<ClientEmail> clientEmail,String uniqueCode) {
        dao.spIngresarDatosClienteEmail(clientEmail, uniqueCode);
        System.out.println("Se REGISTRÓ la información en la tabla ClientEmail");
    }
    public static void spIngresarDatosClientePhone(List<ClientPhone> clientPhone,String uniqueCode) {
        dao.spIngresarDatosClientePhone(clientPhone, uniqueCode);
        System.out.println("Se REGISTRÓ la información en la tabla ClientPhone");
    }

    public static void setNewClienteTarjeta(String tipodocumento, String numerodocumento, String numerotarjeta, String tipotarjeta, String marcatarjeta, String estadotarjeta) throws Exception {
        ClienteTarjeta a = new ClienteTarjeta();
        a.setTipotarjeta(tipodocumento);
        a.setNumerotarjeta(numerodocumento);
        a.setNumerotarjeta(numerotarjeta);
        a.setTipotarjeta(tipotarjeta);
        a.setMarcatarjeta(marcatarjeta);
        a.setEstadoproducto(estadotarjeta);

        dao.sp_insertClienteTarjeta(tipodocumento, numerodocumento, numerotarjeta, tipotarjeta, marcatarjeta, estadotarjeta);
        LOGGER.info("Registro de cliente en BD exitoso");

    }

    public static void setNewUsuario(String Usuario, String Area, String Contrasena, String Rol) throws Exception {
        Usuario a = new Usuario();
        a.setUsuario(Usuario);
        a.setArea(Area);
        a.setContrasena(Contrasena);
        a.setRol(Rol);

        dao.sp_insertUsuario(a.getUsuario(), a.getArea(), a.getContrasena(), a.getRol());
        LOGGER.info("Registro de cliente en BD exitoso");

    }

    public static void updatePasswordUsuario(String Usuario, String Contrasena) throws Exception {
        Usuario a = new Usuario();
        a.setUsuario(Usuario);
        a.setContrasena(Contrasena);
        dao.sp_updatePasswordUsuario(a.getUsuario(), a.getContrasena());
        LOGGER.info("actualizacion de cliente en BD exitoso");

    }

    public static void deleteUsuario(String Usuario) throws Exception {
        Usuario a = new Usuario();
        a.setUsuario(Usuario);
        dao.sp_deleteUsuario(a.getUsuario());
        LOGGER.info("eliminacion de cliente en BD exitoso");

    }


    public static void setNewCliente(String tipodocumento, String nrodocumento, String operador, String telefono, String cantidadTarjetas) throws Exception {
        Cliente a = new Cliente();
        a.setTipodocumento((tipodocumento));
        a.setNrodocumento(setEncrypt(nrodocumento));
        a.setOperador(operador);
        a.setTelefono(setEncrypt(telefono));
        a.setCantidadTarjetas(cantidadTarjetas);
        dao.sp_insertCliente(a.getTipodocumento(), a.getNrodocumento(), a.getOperador(), a.getTelefono(), a.getCantidadTarjetas());
        LOGGER.info("Registro de cliente en BD exitoso");

    }

    public static void updateMillas(String codigoUnico, int millasTarjeta) {
        Cliente a = new Cliente();
        //a.setCodigoUnico(codigoUnico);
        //a.setMillasTarjeta(millasTarjeta);
        //dao.sp_updateMillas(a.getCodigoUnico(), a.getMillasTarjeta());
//        LOGGER.info("Millas actualizadas en BD para el cliente {}", a.getCodigoUnico());

    }

    public static void updateRegistroBenni(String codigoUnico) {
        Cliente a = new Cliente();
        //     a.setCodigoUnico(codigoUnico);
        //    dao.sp_updateRegistroBenni(a.getCodigoUnico());
        LOGGER.info("Registro actualizado");
    }

    public static List<Cliente> getListClientes() {
        return dao.sp_getClientes();
    }

    public static List<ClientData> spGetClientByDocumentType(String tipoDoc) {
        return dao.spGetClientByDocumentType(tipoDoc);
    }

    public static List<TipoGestion> spGetTipoGestionByGestionPadre(String descripcionTipoGestion) {
        return dao.spGetTipoGestionByGestionPadre(descripcionTipoGestion);
    }

    public static List<Cliente> getListUsuariosPorTarjetas(String vcantidadtarjetas) {
        return dao.sp_getClientePorTarjetas(vcantidadtarjetas);
    }


    public static Cliente recargarMillas(Cliente cli) throws InterruptedException {
        //  LOGGER.info("Inicia la recarga de Millas para el cliente {}", cli.getCodigoUnico());

        //loyaltyStep.openLoyalty();
        //loyaltyStep.doLogin();
        // loyaltyStep.verifyLogin();
        // loyaltyStep.doMillas(cli);
        //updateMillas(cli.getCodigoUnico(), cli.getMillasTarjeta());
        return cli;
    }

    public static Cliente crearCliente(Cliente cli) {
        return CrearCliente.crearClienteBus(cli);
    }

    public static String crearToken2() {
        return Token.crearToken();
    }

    public static Cliente consultarDeuda(String token) {
        return ConsultarDeuda.consultarDeuda(token);
    }

    public static Cliente crearTarjeta(Cliente cli) {
        return AltaTarjeta.altaTarjeta(cli);
    }

    public static Cliente clienteRegistroTDM(String tipodocumento, String nrodocumento, String operador, String telefono, String cantidadTarjetas) throws Exception {
        Cliente cli = new Cliente();
        cli.setTipodocumento(tipodocumento);
        cli.setNrodocumento(nrodocumento);
        cli.setOperador(operador);
        cli.setTelefono(telefono);
        setNewCliente(tipodocumento, nrodocumento, operador, telefono, cantidadTarjetas);
        return cli;
    }

    public static Usuario usuarioRegistroTDM(String Usuario, String Area, String Contrasena, String Rol) throws Exception {
        Usuario us = new Usuario();
        us.setUsuario((Usuario));
        us.setArea(Area);
        us.setContrasena((Contrasena));
        us.setRol(Rol);

        setNewUsuario(Usuario, Area, Contrasena, Rol);
        return us;
    }

    public static Cliente crearClienteTarjeta() throws Exception {
        Cliente cli = new Cliente();
        crearCliente(cli);
        crearTarjeta(cli);
        //    setNewCliente(cli.getTipoDocumento(), cli.getNumeroDocumento(), cli.getCodigoUnico(), cli.getTipoTarjeta(), cli.getNumeroTarjeta()
        //          , cli.getFechaCaducidadTarjeta(), 0, "0", "", "");

        return cli;

    }

    public static Usuario getUsuarioPorRolTdm(String rol) throws InterruptedException {

/*        if (cli == null) {
            LOGGER.info(" No existen clientes registrados, se procede a crear Cliente TDM:");
            //TODO: Crear Cliente TDM + Millas
            crearCliente(cli);
            crearTarjeta(cli);
            recargarMillas(cli);
        } else if (cli.getMillasTarjeta() < 10000) {
            LOGGER.info("El cliente tiene pocas millas");
            //recargarMillas(cli);
        }*/


        Usuario cli;
        List<Usuario> list = dao.sp_getUsuarioPorRol(rol).stream().collect(Collectors.toList());
        cli = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
        LOGGER.info("Cliente obtenido de TDM " + cli);
        return cli;

    }


    public static User getUsersPorRolYAmbiente(String rol, String ambiente) throws InterruptedException {

        User cli;
        List<User> list = dao.sp_getUsersPorRolYAmbiente(rol, ambiente).stream().collect(Collectors.toList());
        cli = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
        LOGGER.info("Cliente obtenido de TDM " + cli);
        return cli;

    }
    public static Usuario getUsuarioPorRolYAreaTdm(String rol, String area) throws InterruptedException {

        Usuario cli;
        List<Usuario> list = dao.sp_getUsuarioPorRolYArea(rol, area).stream().collect(Collectors.toList());
        cli = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
        LOGGER.info("Cliente obtenido de TDM " + cli);
        return cli;

    }
    public static RegistroGestion getReclamoFiltros(String tipoRegistro, String tipoDoc, String nroDoc) throws InterruptedException {

        RegistroGestion cli;
        List<RegistroGestion> list = dao.sp_getGestionPorTipoRegistroYDocumento(tipoRegistro, tipoDoc, nroDoc).stream().collect(Collectors.toList());
        cli = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
        LOGGER.info("Gestion obtenida de TDM " + cli);
        return cli;

    }

    //TODO: TIPOLOGIA
    public static ObtenerTipologiaId getTipologiaId(String ambiente, String producto, String tipoTramite, String nombreTipologia) {
        ObtenerTipologiaId cli;
        List<ObtenerTipologiaId> list = dao.sp_getTipologiaId(ambiente, producto, tipoTramite, nombreTipologia).stream().collect(Collectors.toList());
        cli = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
        LOGGER.info("OBTIENE ID DE TIPOLOGIA DESDE TDM " + cli);
        return cli;
    }

    //TODO: MOTIVO
    public static List<ObtenerMotivoId> getMotivoId(String ambiente, String producto, String nombreTipologia) {
        List<ObtenerMotivoId> list = dao.sp_getMotivoId(ambiente, producto, nombreTipologia).stream().collect(Collectors.toList());
        LOGGER.info("OBTIENE ID DE MOTIVO DESDE TDM " + list);
        return list;
    }

    //TODO: TIPOLOGIA COMERCIAL
    public static ObtenerTipologiaIdComercial getTipologiaIdComercial(String ambiente, String producto, String tipoTramite, String nombreTipologia) {
        ObtenerTipologiaIdComercial cli;
        List<ObtenerTipologiaIdComercial> list = dao.sp_getTipologiaIdComercial(ambiente, producto, tipoTramite, nombreTipologia).stream().collect(Collectors.toList());
        cli = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
        LOGGER.info("OBTIENE ID DE TIPOLOGIA DESDE TDM " + cli);
        return cli;
    }

    //TODO: MOTIVO COMERCIAL
    public static List<ObtenerMotivoIdComercial> getMotivoIdComercial(String ambiente, String producto, String nombreTipologia) {
        List<ObtenerMotivoIdComercial> list = dao.sp_getMotivoIdComercial(ambiente, producto, nombreTipologia).stream().collect(Collectors.toList());
        LOGGER.info("OBTIENE ID DE MOTIVO DESDE TDM " + list);
        return list;
    }


    //TODO: RESOLUCION RETAIL
    public static EscenarioValidacion getEscenarioValidacion(String tipoproducto, String tipotramite, String tipologia, String areaValidadoraOpcional) throws InterruptedException {
        EscenarioValidacion cli;
        List<EscenarioValidacion> list = dao.sp_getEscenarioValidacionPorFiltros(tipoproducto, tipotramite, tipologia, areaValidadoraOpcional).stream().collect(Collectors.toList());
        cli = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
        LOGGER.info("RESOLUCION RETAIL DE TDM " + cli);
        return cli;
    }
    //TODO: RESOLUCION COMERCIAL
    public static resolucionComercial getResolucionComercial(String tipoproducto, String tipotramite, String tipologia, String areaValidadoraOpcional) throws InterruptedException {
        resolucionComercial cli;
        List<resolucionComercial> list = dao.sp_getResolucionComercialPorFiltros(tipoproducto, tipotramite, tipologia, areaValidadoraOpcional).stream().collect(Collectors.toList());
        cli = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
        LOGGER.info("RESOLUCION COMERCIAL DE TDM " + cli);
        return cli;
    }

    public static ClienteTarjeta getClienteTarjetaPorFiltroTdm(String tipotarjeta, String marcatarjeta, String estadotarjeta) throws InterruptedException {
        ClienteTarjeta cli;
        List<ClienteTarjeta> list = dao.sp_getClienteTarjetaPorFiltros(tipotarjeta, marcatarjeta, estadotarjeta).stream().collect(Collectors.toList());
        cli = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
        LOGGER.info("Tarjeta obtenido de TDM " + cli);
        return cli;
    }
    public static ClienteTarjeta getClienteDocumentoProductoTdm(String tipoproducto, String tipodocumento) throws InterruptedException {
        ClienteTarjeta cli;
        List<ClienteTarjeta> list = dao.sp_getClienteDocumentoProductoPorFiltros(tipoproducto, tipodocumento).stream().collect(Collectors.toList());
        cli = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
        LOGGER.info("Tarjeta debito obtenido de TDM " + cli);
        return cli;
    }
    public static ClienteTarjeta getClienteDocumentoProductoEstadoTdm(String tipoproducto, String tipodocumento, String tipoestado) throws InterruptedException {
        ClienteTarjeta cli;
        List<ClienteTarjeta> list = dao.sp_getClienteDocumentoProductoEstadoPorFiltros(tipoproducto, tipodocumento, tipoestado).stream().collect(Collectors.toList());
        cli = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
        LOGGER.info("Tarjeta debito obtenido de TDM " + cli);
        return cli;
    }
    public static ClientData getClientePorTipoDoc(String tipodocumento) throws InterruptedException {
        ClientData cli;
        List<ClientData> list = dao.spGetClientePorTipoDoc(tipodocumento).stream().collect(Collectors.toList());
        cli = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
        LOGGER.info("Tarjeta debito obtenido de TDM " + cli);
        return cli;
    }


    public static ClienteTarjeta getClienteTarjetaDebitoPorFiltroTdm(String tipotarjeta, String estadoproducto) throws InterruptedException {
        ClienteTarjeta cli;
        List<ClienteTarjeta> list = dao.sp_getClienteTarjetaDebitoPorFiltros(tipotarjeta, estadoproducto).stream().collect(Collectors.toList());
        cli = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
        LOGGER.info("Cliente obtenido de TDM " + cli);
        return cli;
    }

    public static void PostRegistroGestionTdm(String NroTramite, String Usuario, String FuenteData, String TipoRegistro, String TipoDocumento, String NumDocumento, String IdGestion, String Llamada, String Comentario) throws InterruptedException {
        dao.sp_insertrRegistroGestion(NroTramite, Usuario, FuenteData, TipoRegistro, TipoDocumento, NumDocumento, IdGestion, Llamada, Comentario);

    }

    public static void PostRegistrarReclamoTdm(String NroReclamo, String Tipologia, String EscenarioRegistrado, String EstadoTarjeta, String MarcaTarjeta, String TipoTarjeta, String UsuarioRegistro, String Perfil, String TipoDocumento, String NumeroDocumento, String NumeroTarjeta, String NumeroCuenta, String Tramite, String Atencion, String AgregarComentarios, String RespuestaCliente, String AdjuntarArchivo, String MedioRespuesta, String NumeroCelular, String NuevoRegistroRespuesta, String RazonOperativa,
                                               String TipoCMPMalConcretado,
                                               String Operador,
                                               String FechaConsumoMalConcretado,
                                               String TipoRazonOperativa,
                                               String MotivoSucedidoTarjeta,
                                               String SeleccionarNoBloqueado,
                                               String DescripcionNoBloqueado,
                                               String NombreComercio,
                                               String CantidadTarjetas) throws InterruptedException {
        dao.sp_insertrRegistroReclamo(NroReclamo, Tipologia, EscenarioRegistrado, EstadoTarjeta, MarcaTarjeta, TipoTarjeta, UsuarioRegistro, Perfil, TipoDocumento, NumeroDocumento, NumeroTarjeta, NumeroCuenta, Tramite, Atencion, AgregarComentarios, RespuestaCliente, AdjuntarArchivo, MedioRespuesta, NumeroCelular, NuevoRegistroRespuesta, RazonOperativa,
                TipoCMPMalConcretado,
                Operador,
                FechaConsumoMalConcretado,
                TipoRazonOperativa,
                MotivoSucedidoTarjeta,
                SeleccionarNoBloqueado,
                DescripcionNoBloqueado,
                NombreComercio,
                CantidadTarjetas);

    }

    public static Cliente clienteMillasTdm() throws InterruptedException {
        Cliente cli;
      /*  List<Cliente> list = Do.getListClientes().stream().filter(Cliente::isRegistroBenni).collect(Collectors.toList());
        cli = list.stream().filter(x -> x.getMillasTarjeta() >= 10000).findAny().orElse(list.stream().findAny().orElse(null));
        if (cli == null) {
            LOGGER.info(" No existen clientes registrados, se procede a crear Cliente TDM:");
            //TODO: Crear Cliente TDM + Millas
            crearCliente(cli);
            crearTarjeta(cli);
            recargarMillas(cli);
        } else if (cli.getMillasTarjeta() < 10000) {
            LOGGER.info("El cliente tiene pocas millas");
            //recargarMillas(cli);
        }
        LOGGER.info("Cliente obtenido de TDM " + cli);

        return cli;*/
        return null;
    }


    public static String setEncrypt(String value) throws Exception {
        return encrypt(value);
    }

    public static String getDecrypt(String value) throws Exception {
        return decrypt(value);
    }

    //--------------------------------------------------------------- Renzo


}
