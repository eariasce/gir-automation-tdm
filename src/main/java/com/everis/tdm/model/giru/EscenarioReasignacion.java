package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class EscenarioReasignacion {

    private String Numeroescenario;
    private String User;
    private String Password;
    private String Profile;
    private String MenuPrincipal;
    private String NumeroTramite;
    private String UsuarioADerivarNombre;
    private String PerfilReasignado;
    private String Area;


}
