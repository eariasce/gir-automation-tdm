package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class ClientDataTypeDocument {
    private String uniqueCode;
    private String typeClient;
    private String typeDocument;
    private String documentClient;
    private String firstFirstName;
    private String secondFirstName;
    private String firstLastName;
    private String secondLastName;
    private String phoneNumber;
}
