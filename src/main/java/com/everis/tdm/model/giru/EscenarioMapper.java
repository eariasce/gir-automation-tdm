package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EscenarioMapper implements RowMapper<Escenario> {
    @SneakyThrows
    public Escenario mapRow(ResultSet rs, int arg1) throws SQLException {
        Escenario data = new Escenario();
        data.setNumeroescenario(rs.getString("Numeroescenario"));
        data.setUser(rs.getString("User"));
        data.setPassword(rs.getString("Password"));
        data.setProfile(rs.getString("Profile"));
        data.setTipoDocumento(rs.getString("TipoDocumento"));
        data.setNumeroDocumento(rs.getString("NumeroDocumento"));
        data.setNumeroTarjeta(rs.getString("NumeroTarjeta"));
        data.setTramite(rs.getString("Tramite"));
        data.setTipologia(rs.getString("Tipologia"));
        data.setAtencion(rs.getString("Atencion"));
        data.setPerfil(rs.getString("Perfil"));
        data.setAgregarComentarios(rs.getString("AgregarComentarios"));
        data.setRespuestaCliente(rs.getString("RespuestaCliente"));
        data.setAdjuntarArchivo(rs.getString("AdjuntarArchivo"));
        data.setMedioRespuesta(rs.getString("MedioRespuesta"));
        data.setNumeroCelular(rs.getString("NumeroCelular"));
        data.setNuevoRegistroRespuesta(rs.getString("NuevoRegistroRespuesta"));
        data.setRazonOperativa(rs.getString("RazonOperativa"));
        data.setTipoCMPMalConcretado(rs.getString("TipoCMPMalConcretado"));
        data.setOperador(rs.getString("Operador"));
        data.setFechaConsumoMalConcretado(rs.getString("FechaConsumoMalConcretado"));
        data.setTipoRazonOperativa(rs.getString("TipoRazonOperativa"));
        data.setMotivoSucedidoTarjeta(rs.getString("MotivoSucedidoTarjeta"));
        data.setSeleccionarNoBloqueado(rs.getString("SeleccionarNoBloqueado"));
        data.setDescripcionNoBloqueado(rs.getString("DescripcionNoBloqueado"));
        data.setNombreComercio(rs.getString("NombreComercio"));
        data.setCantidadTarjetas(rs.getString("CantidadTarjetas"));

        return data;
    }
}