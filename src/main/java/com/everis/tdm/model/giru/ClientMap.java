package com.everis.tdm.model.giru;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientMap implements RowMapper<Client> {
    public Client mapRow(ResultSet rs, int arg1) throws SQLException {
        Client client = new Client();
        client.setUniqueCode(rs.getString("uniqueCode"));
        client.setOffice(rs.getString("office"));
        client.setAccountNumber(rs.getString("accountNumber"));
        return client;
    }
}
