package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ObtenerMotivoIdComercialMapper implements RowMapper<ObtenerMotivoIdComercial> {
    @SneakyThrows
    public ObtenerMotivoIdComercial mapRow(ResultSet rs, int arg1) throws SQLException {
        ObtenerMotivoIdComercial data = new ObtenerMotivoIdComercial();

        data.setAmbiente(rs.getString("Ambiente"));
        data.setProducto(rs.getString("Producto"));
        data.setNombreTipologia(rs.getString("NombreTipologia"));
        data.setNombreMotivo(rs.getString("NombreMotivo"));
        data.setIdMotivo(rs.getString("IdMotivo"));
        
        return data;
    }

}