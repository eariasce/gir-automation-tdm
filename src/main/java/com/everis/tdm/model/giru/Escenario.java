package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class Escenario {

    private String Numeroescenario;
    private String User;
    private String Password;
    private String Profile;
    private String TipoDocumento;
    private String NumeroDocumento;
    private String NumeroTarjeta;
    private String Tramite;
    private String Tipologia;
    private String Atencion;
    private String Perfil;
    private String AgregarComentarios;
    private String RespuestaCliente;
    private String AdjuntarArchivo;
    private String MedioRespuesta;
    private String NumeroCelular;
    private String NuevoRegistroRespuesta;
    private String RazonOperativa;
    private String TipoCMPMalConcretado;
    private String Operador;
    private String FechaConsumoMalConcretado;
    private String TipoRazonOperativa;
    private String MotivoSucedidoTarjeta;
    private String SeleccionarNoBloqueado;
    private String DescripcionNoBloqueado;
    private String NombreComercio;
    private String CantidadTarjetas;

}
