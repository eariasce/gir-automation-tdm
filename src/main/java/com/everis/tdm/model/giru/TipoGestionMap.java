package com.everis.tdm.model.giru;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TipoGestionMap implements RowMapper<TipoGestion> {
    public TipoGestion mapRow(ResultSet rs, int arg1) throws SQLException {
        TipoGestion tipoGestion = new TipoGestion();
        tipoGestion.setIdTipoGestion(rs.getString("idTipoGestion"));
        tipoGestion.setDescripcionGestion(rs.getString("descripcionGestion"));
        tipoGestion.setIdTipoGestionHija(rs.getString("idTipoGestionHija"));
        tipoGestion.setDescripcionGestionHija(rs.getString("descripcionGestionHija"));
        return tipoGestion;
    }
}
