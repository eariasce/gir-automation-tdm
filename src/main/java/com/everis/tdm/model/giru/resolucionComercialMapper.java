package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class resolucionComercialMapper implements RowMapper<resolucionComercial> {
    @SneakyThrows
    public resolucionComercial mapRow(ResultSet rs, int arg1) throws SQLException {
        resolucionComercial data = new resolucionComercial();
        data.setTipoProducto(rs.getString("tipoProducto"));
        data.setTipoTramite(rs.getString("tipoTramite"));
        data.setTipologia(rs.getString("tipologia"));
        data.setAreaInicial(rs.getString("areaInicial"));

        data.setAreaValidadoraOpcional(rs.getString("areaValidadoraOpcional"));
        data.setAreaDevuelta(rs.getString("areaDevuelta"));
        data.setAreaResolutora(rs.getString("areaResolutora"));
        data.setFase(rs.getString("fase"));

        return data;
    }

}