package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class resolucionComercial {

    private String tipoProducto;
    private String tipoTramite;
    private String tipologia;
    private String areaInicial;
    private String areaValidadoraOpcional;
    private String areaDevuelta;
    private String areaResolutora;
    private String fase;


}
