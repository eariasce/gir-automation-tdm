package com.everis.tdm.model.giru;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientDataMap implements RowMapper<ClientData> {
    public ClientData mapRow(ResultSet rs, int arg1) throws SQLException {
        ClientData clientData = new ClientData();
        clientData.setUniqueCode(rs.getString("uniqueCode"));
        clientData.setTypeClient(rs.getString("typeClient"));
        clientData.setDocumentClient(rs.getString("documentClient"));
        clientData.setTypeDocument(rs.getString("typeDocument"));
        clientData.setFirstFirstName(rs.getString("firstFirstName"));
        clientData.setSecondFirstName(rs.getString("secondFirstName"));
        clientData.setFirstLastName(rs.getString("firstLastName"));
        clientData.setSecondLastName(rs.getString("secondLastName"));
        clientData.setCellNumber(rs.getString("phoneNumber"));
        return clientData;
    }
}
