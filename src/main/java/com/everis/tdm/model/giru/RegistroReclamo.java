package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class RegistroReclamo {

    private String NroTramite;
    private String Tipologia;
    private String EscenarioRegistrado;
    private String EstadoTarjeta;
    private String MarcaTarjeta;
    private String TipoTarjeta;
    private String UsuarioRegistro;
    private String Perfil;
    private String TipoDocumento;
    private String NumeroDocumento;
    private String NumeroTarjeta;
    private String NumeroCuenta;
    private String Tramite;
    private String Atencion;
    private String AgregarComentarios;
    private String RespuestaCliente;
    private String AdjuntarArchivo;
    private String MedioRespuesta;
    private String NumeroCelular;
    private String NuevoRegistroRespuesta;
    private String Operador;

}
