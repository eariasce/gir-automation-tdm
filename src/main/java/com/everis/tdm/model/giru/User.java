package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class User {

    private String usuario;
    private String contrasena;
    private String rol;
    private String area;
    private String ambiente;
    private String perfil;

}
