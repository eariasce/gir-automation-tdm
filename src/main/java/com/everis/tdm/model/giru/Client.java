package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class Client {
    private String uniqueCode;
    private String office;
    private String accountNumber;
}
