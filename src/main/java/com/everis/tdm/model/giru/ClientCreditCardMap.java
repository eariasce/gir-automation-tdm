package com.everis.tdm.model.giru;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientCreditCardMap implements RowMapper<ClientCreditCard> {
    public ClientCreditCard mapRow(ResultSet rs, int arg1) throws SQLException {
        ClientCreditCard clientCreditCard = new ClientCreditCard();
        clientCreditCard.setUniqueCode(rs.getString("uniqueCode"));
        clientCreditCard.setDocumentClient(rs.getString("documentClient"));
        clientCreditCard.setCardNumber(rs.getString("cardNumber"));
        clientCreditCard.setAccountNumber(rs.getString("accountNumber"));
        clientCreditCard.setTypeDocument(rs.getString("typeDocument"));
        clientCreditCard.setCurrency(rs.getString("currency"));

        return clientCreditCard;
    }
}