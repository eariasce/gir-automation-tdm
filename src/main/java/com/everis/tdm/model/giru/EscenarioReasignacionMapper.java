package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EscenarioReasignacionMapper implements RowMapper<EscenarioReasignacion> {
    @SneakyThrows
    public EscenarioReasignacion mapRow(ResultSet rs, int arg1) throws SQLException {
        EscenarioReasignacion data = new EscenarioReasignacion();
        data.setNumeroescenario(rs.getString("Numeroescenario"));
        data.setUser(rs.getString("User"));
        data.setPassword(rs.getString("Password"));
        data.setProfile(rs.getString("Profile"));

        data.setMenuPrincipal(rs.getString("MenuPrincipal"));
        data.setNumeroTramite(rs.getString("NumeroTramite"));
        data.setUsuarioADerivarNombre(rs.getString("UsuarioADerivarNombre"));
        data.setPerfilReasignado(rs.getString("PerfilReasignado"));
        data.setArea(rs.getString("Area"));
        return data;
    }
}