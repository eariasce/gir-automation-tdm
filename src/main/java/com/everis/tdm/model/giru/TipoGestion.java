package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
@Setter
public class TipoGestion {
    private String idTipoGestion;
    private String descripcionGestion;
    private String idTipoGestionHija;
    private String descripcionGestionHija;
}
