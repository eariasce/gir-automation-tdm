package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class ClientEmail {
    private String uniqueCode;
    private String id;
    private String type;
    private String value;
}
