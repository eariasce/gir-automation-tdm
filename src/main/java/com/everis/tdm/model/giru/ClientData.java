package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
@Setter
public class ClientData {
    private String uniqueCode;
    private String typeClient;
    private String documentClient;
    private String typeDocument;
    private String firstFirstName;
    private String secondFirstName;
    private String firstLastName;
    private String secondLastName;
    private String cellNumber;
    private String profileLevel;
    private String profileLevelStatus;
    private String statusProfileLevel;
    private String profileLevelChange;
    private String segment;
    private String situation;
    private String serviceLevel;
    private String personType;
    private String customerPEP;
    private  List<ClientAddress> address;
    private  ClientCompany company;
    private  List<ClientEmail> email;
    private  List<ClientPhone> phone;

}
