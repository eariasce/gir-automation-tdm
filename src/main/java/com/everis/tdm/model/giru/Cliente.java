package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class Cliente {

    private String tipodocumento;
    private String nrodocumento;
    private String operador;
    private String telefono;
    private String cantidadTarjetas;


}
