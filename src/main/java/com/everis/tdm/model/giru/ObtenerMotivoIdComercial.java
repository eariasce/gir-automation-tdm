package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class ObtenerMotivoIdComercial {
    private String Ambiente;
    private String Producto;
    private String NombreTipologia;
    private String NombreMotivo;
    private String IdMotivo;
    
}
