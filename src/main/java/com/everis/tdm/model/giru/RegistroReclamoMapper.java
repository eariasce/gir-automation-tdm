package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.everis.tdm.security.Decryption.decrypt;

public class RegistroReclamoMapper implements RowMapper<RegistroReclamo> {
    @SneakyThrows
    public RegistroReclamo mapRow(ResultSet rs, int arg1) throws SQLException {
        RegistroReclamo data = new RegistroReclamo();

        data.setNroTramite(rs.getString("NroTramite"));
        data.setTipologia(rs.getString("Tipologia"));

        data.setEscenarioRegistrado(rs.getString("EscenarioRegistrado"));
        data.setEstadoTarjeta(rs.getString("EstadoTarjeta"));

        data.setMarcaTarjeta(rs.getString("MarcaTarjeta"));
        data.setTipoTarjeta(rs.getString("TipoTarjeta"));

        data.setUsuarioRegistro(rs.getString("UsuarioRegistro"));
        data.setPerfil(rs.getString("Perfil"));

        data.setTipoDocumento(rs.getString("TipoDocumento"));
        data.setNumeroDocumento(rs.getString("NumeroDocumento"));

        data.setNumeroTarjeta(rs.getString("NumeroTarjeta"));
        data.setNumeroCuenta(rs.getString("NumeroCuenta"));

        data.setTramite(rs.getString("Tramite"));
        data.setAtencion(rs.getString("Atencion"));

        data.setAgregarComentarios(rs.getString("AgregarComentarios"));
        data.setRespuestaCliente(rs.getString("RespuestaCliente"));

        data.setAdjuntarArchivo(rs.getString("AdjuntarArchivo"));
        data.setMedioRespuesta(rs.getString("MedioRespuesta"));

        data.setNumeroCelular(rs.getString("NumeroCelular"));
        data.setNuevoRegistroRespuesta(rs.getString("NuevoRegistroRespuesta"));

        data.setOperador(rs.getString("Operador"));


        return data;
    }
}



