package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ObtenerMotivoIdMapper implements RowMapper<ObtenerMotivoId> {
    @SneakyThrows
    public ObtenerMotivoId mapRow(ResultSet rs, int arg1) throws SQLException {
        ObtenerMotivoId data = new ObtenerMotivoId();

        data.setAmbiente(rs.getString("Ambiente"));
        data.setProducto(rs.getString("Producto"));
        data.setNombreTipologia(rs.getString("NombreTipologia"));
        data.setNombreMotivo(rs.getString("NombreMotivo"));
        data.setIdMotivo(rs.getString("IdMotivo"));
        
        return data;
    }

}