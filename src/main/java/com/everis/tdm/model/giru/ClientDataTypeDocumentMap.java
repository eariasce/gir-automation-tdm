package com.everis.tdm.model.giru;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientDataTypeDocumentMap implements RowMapper<ClientDataTypeDocument> {
    public ClientDataTypeDocument mapRow(ResultSet rs, int arg1) throws SQLException {
        ClientDataTypeDocument clientDataTypeDocument = new ClientDataTypeDocument();
        clientDataTypeDocument.setUniqueCode(rs.getString("uniqueCode"));
        clientDataTypeDocument.setTypeClient(rs.getString("typeClient"));
        clientDataTypeDocument.setTypeDocument(rs.getString("typeDocument"));
        clientDataTypeDocument.setDocumentClient(rs.getString("documentClient"));
        clientDataTypeDocument.setFirstFirstName(rs.getString("firstFirstName"));
        clientDataTypeDocument.setSecondFirstName(rs.getString("secondFirstName"));
        clientDataTypeDocument.setFirstLastName(rs.getString("firstLastName"));
        clientDataTypeDocument.setSecondLastName(rs.getString("secondLastName"));
        clientDataTypeDocument.setPhoneNumber(rs.getString("phoneNumber"));
        return clientDataTypeDocument;
    }
}
