package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.everis.tdm.security.Decryption.decrypt;

public class ReclamoMapper implements RowMapper<Reclamo> {
    @SneakyThrows
    public Reclamo mapRow(ResultSet rs, int arg1) throws SQLException {
        Reclamo data = new Reclamo();
        data.setTipologia(decrypt(rs.getString("tipologia")));
        data.setMedioRespuesta(decrypt(rs.getString("medioRespuesta")));
        return data;
    }
}



