package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ObtenerTipologiaIdMapper implements RowMapper<ObtenerTipologiaId> {
    @SneakyThrows
    public ObtenerTipologiaId mapRow(ResultSet rs, int arg1) throws SQLException {
        ObtenerTipologiaId data = new ObtenerTipologiaId();
        
        data.setAmbiente(rs.getString("Ambiente"));
        data.setProducto(rs.getString("Producto"));
        data.setTipoTramite(rs.getString("TipoTramite"));
        data.setNombreTipologia(rs.getString("NombreTipologia"));
        data.setIdTipologiaElement(rs.getString("IdTipologiaElement"));
         return data;
    }

}