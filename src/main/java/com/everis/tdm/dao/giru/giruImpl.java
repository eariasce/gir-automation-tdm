package com.everis.tdm.dao.giru;

import com.everis.tdm.model.giru.Cliente;
import com.everis.tdm.model.giru.Usuario;

import java.util.List;

public interface giruImpl {

    List<Usuario> sp_getUsuarioPorRol(String vrol);

    void sp_insertCliente(String tipodocumento, String nrodocumento, String operador, String telefono, String cantidadtarjetas);

    void sp_updateMillas(String codigoUnico, int millasTarjeta);

    List<Cliente> sp_getClientePorTarjetas(String vcantidadtarejtas);

    void sp_updateRegistroBenni(String codigoUnico);

    void sp_insertrRegistroReclamo(String NroReclamo, String Tipologia, String EscenarioRegistrado, String EstadoTarjeta, String MarcaTarjeta, String TipoTarjeta, String UsuarioRegistro, String Perfil, String TipoDocumento, String NumeroDocumento, String NumeroTarjeta, String NumeroCuenta, String Tramite, String Atencion, String AgregarComentarios, String RespuestaCliente, String AdjuntarArchivo, String MedioRespuesta, String NumeroCelular, String NuevoRegistroRespuesta, String RazonOperativa, String TipoCMPMalConcretado, String Operador, String FechaConsumoMalConcretado, String TipoRazonOperativa, String MotivoSucedidoTarjeta, String SeleccionarNoBloqueado, String DescripcionNoBloqueado, String NombreComercio, String CantidadTarjetas);

    List<Cliente> sp_getClientes();

    List<Usuario> sp_getUsuarioPorRolYArea(String vrol, String varea);
}
