package com.everis.tdm.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

public class PropertyManager {

    private static Properties instance = null;
    private static final String APPLICATION_PREFIX = "application";
    private static final String APPLICATION_SUFFIX = "properties";

    public PropertyManager() {
    }

    public static synchronized Properties getInstance() {
        if (instance == null) {
            instance = loadPropertiesFile();
        }
        return instance;
    }
    private static Properties loadPropertiesFile() {
        String environment = Optional.ofNullable(System.getProperty("environment")).orElse("uat");
        String fileName = String.format("%s-%s.%s", APPLICATION_PREFIX, environment, APPLICATION_SUFFIX);
        Properties prop = new Properties();
        try {
            //Extrae variables de archivo local
            prop.load(new FileInputStream(System.getProperty("user.home") + File.separator + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }
}