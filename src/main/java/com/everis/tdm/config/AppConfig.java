package com.everis.tdm.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import javax.sql.DataSource;

@ComponentScan("com.everis.tdm")
@Configuration
@PropertySource(value = "application.properties", ignoreResourceNotFound = true)
public class AppConfig {

    @Autowired
    public Environment env;

    @Getter
    private static String key;

    @Value("${secret.key}")
    public void setKey(String key) {
        AppConfig.key = key;
    }

    String dbURL = "db.url";
    String dbUserName = "db.username";
    String dbPassword = "db.password";

    @Bean
    public DataSource dataSource() {
        //  DriverManagerDataSource dataSource = new DriverManagerDataSource();
        SQLServerDataSource dataSource = new SQLServerDataSource();
        //    dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.sqlserverdriver");
        // dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        // dataSource.setUrl(env.getProperty(dbURL));
        dataSource.setServerName(env.getProperty(dbURL));
        // dataSource.setUsername(env.getProperty(dbUserName));
        dataSource.setUser(env.getProperty(dbUserName));
        dataSource.setPassword(env.getProperty(dbPassword));
        dataSource.setDatabaseName("giru_db");

        return dataSource;
    }

}