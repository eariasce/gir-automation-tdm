package com.everis.tdm.services;

import com.everis.tdm.model.giru.Cliente;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.restassured.RestAssured.given;


public class ConsultarDeuda {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultarDeuda.class);

    static private final String CREAR_CLIENTE_URL = "https://apidev.digital.interbank.pe/billpayments/v2/recipients/01021/services/01/bills?clientId=0AGO0445";

    // static private final String TEMPLATE_CREAR_CLIENTE = "/request/crearCliente.xml";

    public static Cliente consultarDeuda(String token) {
        Cliente prueba = new Cliente();
        LOGGER.info("Customer Creation started...");

        RestAssured.useRelaxedHTTPSValidation();
        //    cli.setTipoDocumento("DNI");
        //    cli.setNumeroDocumento(String.valueOf(Util.generateDocumentRandom()));
        // TODO: Controlar cantidad de intentos, evitar saturar el servicio
        while (true) {
            //      String bodyRequest = getTemplate(TEMPLATE_CREAR_CLIENTE).replace("{numDocumento}", "cli.getNumeroDocumento()");
            Response res = given()
                    .header("X-Api-Version", "v2")
                    .header("Ocp-Apim-Subscription-Key", "332a09a749cd4b0eb2d760af339d569f")
                    .header("Authorization", "Bearer " + token).
                    formParam("grant_type", "client_credentials").

                    formParam("scope", "token:application").
                    //            body(bodyRequest).
                            when().
                    get(CREAR_CLIENTE_URL);

            if (res.statusCode() == 200) {
                String body = res.getBody().asString();
                System.out.println("se obtuvo cliente: " + body);


                String name = res.getBody().jsonPath().getString("client.name");
                String id = res.getBody().jsonPath().getString("client.id");
                String bills = res.getBody().jsonPath().getString("bills[0].currency");


                prueba.setNrodocumento(name);
                prueba.setTipodocumento(id);
                prueba.setTelefono(bills);
                prueba.setOperador("CLARO");
                break;
            } else {
                String body = res.getBody().asString();
                System.out.println("se obtuvo cliente: " + body);

                //  LOGGER.info("Request error:  DEBUG MODE");
            }
            LOGGER.info("CREACION DESACTIVADA");
        }
        return prueba;
    }

}
