package com.everis.tdm.utils;


public class Configuration {
    public void setearAmbiente(String scope, String zona, String ambiente, String catalogo, String canal){
        ApiCommons.SCOPE = CommonUtil.scope(scope);
        ApiCommons.AMBIENTE = ambiente;
        ApiCommons.ZONA = zona;
        ApiCommons.CATALOGO = catalogo;
        ApiCommons.CANAL = canal;
        if (ApiCommons.ZONA.equals("sspci")) {
            if (ApiCommons.AMBIENTE.equals("uat")) {
                if (ApiCommons.CATALOGO.equals("smp")) {
                    ApiCommons.URL_BASE = "https://dpsuat.grupoib.local:7300";
                }
                if (ApiCommons.CATALOGO.equals("bpi")) {
                    ApiCommons.URL_BASE = "https://dpiuat.grupoib.local:7020";
                }
                if (ApiCommons.CATALOGO.equals("com")) {
                    ApiCommons.URL_BASE = "https://dpsuat.grupoib.local:7330";
                }
                ApiCommons.CREDENTIAL_USERNAME = "uBseGenNPUat";
                ApiCommons.CREDENTIAL_PASSWORD = "A5k=QPdR";
                ApiCommons.PCI_AUTH = "dUJzZUdlblVhdDpJYmsyMDE4JA==";
            }
        }
        if (ApiCommons.AMBIENTE.equals("uat")) {
            if (ApiCommons.CATALOGO.equals("smp")) {
                ApiCommons.URL_BASE = "https://dpsuat.grupoib.local:7300";
                ApiCommons.OPC_APIM_SUSCRIPTION_KEY = "9442c4ed0a7737f9a3fa3b4b7babf440";
                ApiCommons.OPC_APIM_SUSCRIPTION_SECRECT = "e3a806fd363b69b3f7b43eceac289d5c";
                ApiCommons.AUTHORIZATION_TOKEN_VALUE = "AAIgOTQ0MmM0ZWQwYTc3MzdmOWEzZmEzYjRiN2JhYmY0NDARG34Dx2H3wGha3p1n63Hdq-R4dC2FpJQPokyNpJ20M-i6RkebYrE8aWOg4XkzXx84wXi7dbxGIrlgqr9nL8YRBtG2mPskzBxaG-rrGIBZhQ";
            }
            if (ApiCommons.CATALOGO.equals("bpi")) {
                ApiCommons.URL_BASE = "https://dpiuat.grupoib.local:7020";
                ApiCommons.OPC_APIM_SUSCRIPTION_KEY = "c0d1d5a2a2465d54a9f9bb62e3fe4c14";
                ApiCommons.OPC_APIM_SUSCRIPTION_SECRECT = "5d1625030c49059004fef94b22cc8342";
                ApiCommons.AUTHORIZATION_TOKEN_VALUE = "AAIgOTQ0MmM0ZWQwYTc3MzdmOWEzZmEzYjRiN2JhYmY0NDBC_jeRpx4gAvTCbRDH8pa2TyP4LYJGkEHKa2KXLl2QYcpUju4Uz-Uht3MrM-JZrm4kqf0SLqZSiyYIs5-Xcz7vcYiB0nDgXdD5YiA6sNfwbw";
            }
            if (ApiCommons.CATALOGO.equals("bpifull")) {
                ApiCommons.URL_BASE = "https://dpiuat.grupoib.local:7020";//"http://52.184.213.69:8080";
                ApiCommons.OPC_APIM_SUSCRIPTION_KEY = "c0d1d5a2a2465d54a9f9bb62e3fe4c14";
                ApiCommons.OPC_APIM_SUSCRIPTION_SECRECT = "5d1625030c49059004fef94b22cc8342";
                ApiCommons.AUTHORIZATION_TOKEN_VALUE = "AAIgOTQ0MmM0ZWQwYTc3MzdmOWEzZmEzYjRiN2JhYmY0NDBC_jeRpx4gAvTCbRDH8pa2TyP4LYJGkEHKa2KXLl2QYcpUju4Uz-Uht3MrM-JZrm4kqf0SLqZSiyYIs5-Xcz7vcYiB0nDgXdD5YiA6sNfwbw";
            }
            if (ApiCommons.CATALOGO.equals("com")) {
                ApiCommons.URL_BASE = "https://dpsuat.grupoib.local:7330";
                ApiCommons.OPC_APIM_SUSCRIPTION_KEY = "837db024e0fdad63b23cb4390fd99189";
                ApiCommons.OPC_APIM_SUSCRIPTION_SECRECT = "fa1202a83b914fa8ade40d79796c764b";
                ApiCommons.AUTHORIZATION_TOKEN_VALUE = "AAIgNjA4NmEzYjU5MThkNDEwMGQ2YWVlMTlkMDM0YjVlNTYZI-BRPV-0k6LTuaz-nDgL2Y5_o1Z5eI-KQkzvl97XctvT_IhjCbiNQaYMhy95U0nLjK1qRNnKYc7gb2gb9vUcXoipjqhGPb_m11cFNRNZ4g";
            }
            ApiCommons.CREDENTIAL_USERNAME = "uBseGenNPUat";
            ApiCommons.CREDENTIAL_PASSWORD = "A5k=QPdR";
        }
        //ApiCommons.PATH_AMBIENTE = "/ibk-" + ApiCommons.AMBIENTE;
       // ApiCommons.PATH_CATALOGO = "/int-" + ApiCommons.CATALOGO + "-" + ApiCommons.AMBIENTE;
        ApiCommons.URL_BASE = ApiCommons.URL_BASE + ApiCommons.PATH_AMBIENTE + ApiCommons.PATH_CATALOGO;
    }
}

