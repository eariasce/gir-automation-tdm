package com.everis.tdm.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class CommonUtil {

    public static String scope(String scopeTemp){
        String scope = "";
        if (scopeTemp.toUpperCase().equals("CONSULTA")){
            scope = "scopeConsultaH1";
        }
        if (scopeTemp.toUpperCase().equals("TRANSACCIONAL")){
            scope = "scopeTransaccionH1";
        }
        return scope;
    }


    //Obtener dinamicamente el TraceId y Parentid
    public static String obtenerUUIDv4(int cant){
        String idDinamic = "";

        for (int t=0;t<cant;t++){
            // Generate random number
            double rand = Math.random()*16;

            int k = (int)Math.floor(rand);

            idDinamic = idDinamic+Integer.toHexString(k);
        }

        return idDinamic;
    }


    //Obtener dinamicamente el messageId y timestamp
    public static String obtenerTimeFormat(String tipo){
        String Timeformat = "";
        SimpleDateFormat formatter;
        Date date = new Date(System.currentTimeMillis());

        if(tipo.equals("timeStamp")) {
            formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            Timeformat = formatter.format(date)+"Z";

        }
        else if (tipo.equals("messageId")){
            formatter = new SimpleDateFormat("yyyyMMddHHmmssZZZZZ");
            Timeformat = formatter.format(date);
        }

        return Timeformat;
    }


    //Obtener dinamicamente el IP
    public static String obtenerIp(){
        Random r = new Random();

        String ip = "192."+r.nextInt(256)+"."+r.nextInt(256)+"."+r.nextInt(256);
        return ip;
    }

    public static String truncarNumeroTarjeta(String marcaTarjeta, String numeroTarjeta) {
        String primeraParte = "";
        String segundoParte = "";
        StringBuilder truncados = new StringBuilder();
        int cantidadValores = 0;
        if (marcaTarjeta.equalsIgnoreCase("VISA")) {
            primeraParte = numeroTarjeta.substring(0, 6);
            segundoParte = numeroTarjeta.substring(12, 16);
        }
        if (marcaTarjeta.equalsIgnoreCase("AMEX")) {
            primeraParte = numeroTarjeta.substring(0, 6);
            segundoParte = numeroTarjeta.substring(11, 15);
        }
        if (marcaTarjeta.equalsIgnoreCase("MASTERCARD")) {
            primeraParte = numeroTarjeta.substring(0, 6);
            segundoParte = numeroTarjeta.substring(12, 16);
        }
        cantidadValores = numeroTarjeta.length() - (primeraParte.length() + segundoParte.length());
        for (int i = 0; i < cantidadValores; i++) {
            truncados.append("*");
        }
        System.out.println("marcaTarjeta: " + marcaTarjeta);
        System.out.println("numeroTarjeta: " + numeroTarjeta);
        System.out.println("return: " + primeraParte + truncados + segundoParte);
        return primeraParte + truncados + segundoParte;
    }
}
