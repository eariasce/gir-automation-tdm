package com.everis.tdm.utils;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ApiCommons {
    public static String URL_BASE = "";
    public static String CANAL = "";
    public static String AMBIENTE = "";
    public static String CATALOGO = "";
    public static String AUTHORIZATION_TOKEN_VALUE = "";
    public static String PCI_AUTH="";
    public static String CLIENT_ID = "";
    public static String CLIENT_SECRET = "";
    public static String ZONA = "";
    public static String OPC_APIM_SUSCRIPTION_KEY = "";
    public static String OPC_APIM_SUSCRIPTION_SECRECT = "";
    public static String CREDENTIAL_USERNAME= "";
    public static String CREDENTIAL_PASSWORD= "";
    public static String CREDENTIAL_PCIAUTH= "";
    public static String PATH_AMBIENTE = "";
    public static String PATH_CATALOGO = "";
    public static String SCOPE = "";

    public static String URL_NEW_RELIC="https://insights-api.newrelic.com";

    public static String TRACE_ID="";
    public static String affiliationNumber="";
    public static String PARENT_ID=CommonUtil.obtenerUUIDv4(8);
    public static String TIME_STAMP=CommonUtil.obtenerTimeFormat("timeStamp");
    public static String MESSAGE_ID=CommonUtil.obtenerTimeFormat("messageId");
    public static String ORIGEN_IP=CommonUtil.obtenerIp();

    public static String USERID= "";
    public static final String API_APPLICATION_HEADER = "X-Application-Id";
    public static final String API_CORRELATION_HEADER = "X-Correlation-Id";
    public static final String API_FORCE_SYNC_HEADER = "X-Api-Force-Sync";
    public static final String API_CONTENT_TYPE_HEADER = "Content-Type";
    public static final String API_VERSION = "X-Api-Version";
    public static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";
    public static final String API_OTP = "X-OTP";
    public static final String API_ORIGIN_HEADER = "X-Origin-Id";
    private static final String KEY_CORRELATION_ID = "correlationID";
    public static final String CORRELATION_STATUS_OK = "SETTLED";
    public static final String CORRELATION_STATUS_ERROR = "FAILED";
    public static final int STATUS_CODE_OK = 200;
    public static final int STATUS_CODE_CREATED = 201;
    public static final int ACCEPTED = 202;
    public static final int NO_CONTENT = 204;
    public static final int ALREADY_REPORTED = 208;
    public static final int STATUS_CODE_BAD_REQUEST = 400;
    public static final int STATUS_CODE_UNAUTHORIZED = 401;
    public static final int STATUS_CODE_FORBIDDEN = 403;
    public static final int STATUS_CODE_NOT_FOUND = 404;
    public static final int STATUS_CODE_NOT_ACCEPTABLE = 406;
    public static final int STATUS_CODE_CONFLICT = 409;
    public static final int STATUS_CODE_PRECONDITION_FAILED = 412;
    public static final int STATUS_CODE_UNPROCESSABLE_ENTITY = 422;
    public static final int STATUS_CODE_TOO_MANY_REQUESTS = 429;
    public static final int STATUS_CODE_INTERNAL_SERVER_ERROR = 500;
    public static final int STATUS_CODE_SERVICE_UNAVAILABLE = 503;

    //public static List<AccountCreditCardFullData> accountCreditCardFullData;
    public static String [][] atributosValores;

    public static String CARD_ID_TYPE;

    public static String getTemplate(String templatePath) {
        try {
            return IOUtils.toString(new ClassPathResource(templatePath).getInputStream(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void setApplicationAccessToken(String zona,String canal) {
        configureApplicationAccessToken(zona, canal);
    }


    private static void configureApplicationAccessToken(String zona,String canal) {
        if (zona.equals("PCI")) {
            switch (canal.toUpperCase()) {
                case "BPI":
                    CLIENT_ID = "c0d1d5a2a2465d54a9f9bb62e3fe4c14";
                    CLIENT_SECRET = "5d1625030c49059004fef94b22cc8342";
                    AUTHORIZATION_TOKEN_VALUE = "AAIgOTQ0MmM0ZWQwYTc3MzdmOWEzZmEzYjRiN2JhYmY0NDBC_jeRpx4gAvTCbRDH8pa2TyP4LYJGkEHKa2KXLl2QYcpUju4Uz-Uht3MrM-JZrm4kqf0SLqZSiyYIs5-Xcz7vcYiB0nDgXdD5YiA6sNfwbw";
                    break;

                case "APP":
                    CLIENT_ID = "9442c4ed0a7737f9a3fa3b4b7babf440";
                    CLIENT_SECRET = "e3a806fd363b69b3f7b43eceac289d5c";
                    AUTHORIZATION_TOKEN_VALUE = "AAIgOTQ0MmM0ZWQwYTc3MzdmOWEzZmEzYjRiN2JhYmY0NDARG34Dx2H3wGha3p1n63Hdq-R4dC2FpJQPokyNpJ20M-i6RkebYrE8aWOg4XkzXx84wXi7dbxGIrlgqr9nL8YRBtG2mPskzBxaG-rrGIBZhQ";
                    break;

                case "COM":
                    //URL_BASE = "https://apic-sbx-gw-com-gateway-apic.apps.ocpsbx.grupoib.local/ibk-dev/int-com-dev";
                    CLIENT_ID = "6086a3b5918d4100d6aee19d034b5e56";
                    CLIENT_SECRET = "87be34d6f0885dbb86baa53adebced69";
                    AUTHORIZATION_TOKEN_VALUE = "AAIgNjA4NmEzYjU5MThkNDEwMGQ2YWVlMTlkMDM0YjVlNTYZI-BRPV-0k6LTuaz-nDgL2Y5_o1Z5eI-KQkzvl97XctvT_IhjCbiNQaYMhy95U0nLjK1qRNnKYc7gb2gb9vUcXoipjqhGPb_m11cFNRNZ4g";
                    //CATALOGO = "COM";
                    break;

                default:
                    //
            }

        } else if (zona.equals("SSPCI")) {
            switch (canal.toUpperCase()) {
                case "BPI":
                    CLIENT_ID = "c0d1d5a2a2465d54a9f9bb62e3fe4c14";
                    CLIENT_SECRET = "5d1625030c49059004fef94b22cc8342";
                    AUTHORIZATION_TOKEN_VALUE = "AAIgYzBkMWQ1YTJhMjQ2NWQ1NGE5ZjliYjYyZTNmZTRjMTSAG4gkK-frp5CVb3wjqX1pgPjeqPEwqPIUe7uAV0vOSi6rJUWYVZvb_NMN16eeAz4f915RkzFZlFc7EkQeW_wXTb5oJpMRjDcTEJzZF0Z2AA";
                    PCI_AUTH = "dUJzZUdlblVhdDpJYmsyMDE4JA==";
                    break;
                case "APP":
                    //URL_BASE = "https://dpsuat.grupoib.local:7300/ibk-uat/int-smp-uat";
                    //URL_BASE = "https://dpsuat.grupoib.local:7300/ibk-uat/int-smp-uat";
                    CLIENT_ID = "9442c4ed0a7737f9a3fa3b4b7babf440";
                    CLIENT_SECRET = "e3a806fd363b69b3f7b43eceac289d5c";
                    AUTHORIZATION_TOKEN_VALUE = "AAIgOTQ0MmM0ZWQwYTc3MzdmOWEzZmEzYjRiN2JhYmY0NDARG34Dx2H3wGha3p1n63Hdq-R4dC2FpJQPokyNpJ20M-i6RkebYrE8aWOg4XkzXx84wXi7dbxGIrlgqr9nL8YRBtG2mPskzBxaG-rrGIBZhQ";
                    PCI_AUTH = "dUJzZUdlblVhdDpJYmsyMDE4JA==";
                    break;
                case "COM":
                    //URL_BASE = "https://apic-sbx-gw-com-gateway-apic.apps.ocpsbx.grupoib.local/ibk-dev/int-com-dev";
                    CLIENT_ID = "6086a3b5918d4100d6aee19d034b5e56";
                    CLIENT_SECRET = "87be34d6f0885dbb86baa53adebced69";
                    AUTHORIZATION_TOKEN_VALUE = "AAIgNjA4NmEzYjU5MThkNDEwMGQ2YWVlMTlkMDM0YjVlNTYZI-BRPV-0k6LTuaz-nDgL2Y5_o1Z5eI-KQkzvl97XctvT_IhjCbiNQaYMhy95U0nLjK1qRNnKYc7gb2gb9vUcXoipjqhGPb_m11cFNRNZ4g";
                    //CATALOGO = "COM";
                    break;
                default:
                    //
            }
        } else if (zona.equals("COM")) {
            switch (canal.toUpperCase()) {
                case "COM":
                    //URL_BASE = "https://apic-sbx-gw-com-gateway-apic.apps.ocpsbx.grupoib.local/ibk-dev/int-com-dev";
                    CLIENT_ID = "6086a3b5918d4100d6aee19d034b5e56";
                    CLIENT_SECRET = "87be34d6f0885dbb86baa53adebced69";
                    AUTHORIZATION_TOKEN_VALUE = "AAIgNjA4NmEzYjU5MThkNDEwMGQ2YWVlMTlkMDM0YjVlNTYZI-BRPV-0k6LTuaz-nDgL2Y5_o1Z5eI-KQkzvl97XctvT_IhjCbiNQaYMhy95U0nLjK1qRNnKYc7gb2gb9vUcXoipjqhGPb_m11cFNRNZ4g";
                    //CATALOGO = "COM";
                    break;
                default:
                    //
            }

        }

    }

}

