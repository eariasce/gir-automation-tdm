package com.everis.tdm.utils;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class QueryConsultaCliente {

    public String bodyRequest;
    public Response response;
    static private final String TEMPLATE_CLIENTE_CONSULTA = "/request/consultarCliente.xml";

    public static String getTemplate(String templatePath) {
        try {
            return IOUtils.toString(new ClassPathResource(templatePath).getInputStream(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void seArmaElCuerpoDePeticionConsultarCliente(String tipoDoc, String numDoc) {
        RestAssured.useRelaxedHTTPSValidation();
        bodyRequest = getTemplate(TEMPLATE_CLIENTE_CONSULTA).replace("{tipoDoc}",tipoDoc);
        bodyRequest = getTemplate(TEMPLATE_CLIENTE_CONSULTA).replace("{numDoc}",numDoc);
    }

    @Step("construyo request y hago POST")
    public void agregoLosEncabezadosEnLaPeticion() {
        response = given().log().all().
                header("Accept-Encoding", "gzip,deflate").
                header("Content-Type", "text/xml;charset=UTF-8").
                header("SOAPAction", "http://interbank.com.pe/srv/generic/messageFormat/v4.0/").
                header("Host: dpiuat.grupoib.local","7200").
                header("Connection","Keep-Alive").
                header("User-Agent","Apache-HttpClient/4.5.5 (Java/12.0.1)").
                body(bodyRequest).
                when().post("https://dpiuat.grupoib.local:7200/ibk/srv/MPO/AtencionCliente/cliente.consultarCliente/v1.0");
    }

    public void validoLaRespuestaObtenida() {
        if (response.statusCode() != 200) {
            System.out.println("El servicio de 'CONSULTA CLIENTE'" + " NO RESPONDE ");
        }
        System.out.println(response.body().toString());
        assertThat(response.statusCode(), is(200));
    }
}
