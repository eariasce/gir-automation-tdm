package com.everis.tdm.utils;

import com.everis.tdm.commons.UploadData;
import com.everis.tdm.configurations.ServicesConfiguration;
import com.everis.tdm.model.*;
import com.everis.tdm.model.giru.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class QueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UploadData.class);

    public static String verifyNewDni(String documentNumber, String tipo) {
        System.out.println("()\\ Verificiar si el documento ingresado es de un cliente: " + documentNumber);
        String respuestaNewCliente = "NO_CLIENTE";
        String token = Security.createAccessToken();
        Response response = SerenityRest.given().header("Authorization", token)
                .header("Ocp-Apim-Subscription-Key", ServicesConfiguration.getCustomerSubscriptionKey())
                .header("application", "ASSI")
                .header("company", "INTERBANK")
                .header("storeCode", "100")
                .header("user", "B36199")
                .param("identityDocumentType", tipo)
                .param("identityDocumentNumber", documentNumber)
                .get(ServicesConfiguration.getBaseUrl() + "/vision-cliente/uat/customers/full");
        Security.deleteToken(token);
        if (response.getStatusCode() == 200) {
            LOGGER.info("[CONSULTA CLIENTE] Response: {}", response.getBody().asString());
            respuestaNewCliente = response.jsonPath().getString("customer.personType");
            System.out.println(respuestaNewCliente);
        }else{
            LOGGER.info("[STATUS] Response:{} {}",response.getStatusCode(), response.getBody().asString());
        }
        return respuestaNewCliente;
    }
    public static ClientData getClientData(String documentNumber, String tipo) {
        ClientData clientData = new ClientData();
        String token = Security.createAccessToken();
        Response response = SerenityRest.given().header("Authorization", token)
                .header("Ocp-Apim-Subscription-Key", ServicesConfiguration.getCustomerSubscriptionKey())
                .header("application", "ASSI")
                .header("company", "INTERBANK")
                .header("storeCode", "100")
                .header("user", "B36199")
                .param("identityDocumentType", tipo)
                .param("identityDocumentNumber", documentNumber)
                .get(ServicesConfiguration.getBaseUrl() + "/vision-cliente/uat/customers/full");
        Security.deleteToken(token);
        LOGGER.info("[CONSULTA CLIENTE] Response: {}", response.getBody().asString());
        if (response.getStatusCode() == 200) {
            clientData.setTypeClient(response.jsonPath().getString("customer.personType"));
            clientData.setDocumentClient(response.jsonPath().getString("customer.identityDocument.number"));
            clientData.setTypeDocument(UtilChange.changeDocumentType(response.jsonPath().getString("customer.identityDocument.type")));
            clientData.setUniqueCode(response.jsonPath().getString("customer.id"));
            clientData.setFirstFirstName(response.jsonPath().getString("customer.firstName"));
            clientData.setSecondFirstName(response.jsonPath().getString("customer.middleName"));
            clientData.setFirstLastName(response.jsonPath().getString("customer.lastName"));
            clientData.setSecondLastName(response.jsonPath().getString("customer.motherLastName"));
            clientData.setCellNumber(response.jsonPath().getString("customer.phone.number").replace("[", "").replace("]", ""));

            clientData.setSegment(response.jsonPath().getString("customer.segment"));
            clientData.setSituation(response.jsonPath().getString("customer.situation"));
            clientData.setServiceLevel(response.jsonPath().getString("customer.serviceLevel"));
            clientData.setPersonType(response.jsonPath().getString("customer.personType"));


            clientData.setAddress(response.jsonPath().getList("customer.address"));
            clientData.setPhone(response.jsonPath().getList("customer.phone"));
            clientData.setEmail(response.jsonPath().getList("customer.email"));

            ClientCompany clientCompany=new ClientCompany();
            clientCompany.setUniqueCode(response.jsonPath().getString("customer.id"));
            clientCompany.setName(response.jsonPath().getString("customer.company.name"));
            clientCompany.setAdmissionDate(response.jsonPath().getString("customer.company.admissionDate"));
            clientCompany.setAddress(response.jsonPath().getString("customer.company.address"));
            clientCompany.setPhoneNumber(response.jsonPath().getString("customer.company.phoneNumber"));
            clientCompany.setExtension(response.jsonPath().getString("customer.company.extension"));
            clientCompany.setPositionDescripcion(response.jsonPath().getString("customer.company.positionDescripcion"));
            clientCompany.setOccupation(response.jsonPath().getString("customer.company.occupation"));
            clientCompany.setIndependent(response.jsonPath().getString("customer.company.independent"));
            clientCompany.setUbigeo(response.jsonPath().getString("customer.company.ubigeo"));
            clientCompany.setEmployeeIndicator(response.jsonPath().getString("customer.company.employeeIndicator"));
            clientData.setCompany(clientCompany);
        }
        return clientData;
    }


    private static List<String> situationList;
    private static List<String> statusList;
    private static String cardNumber;
    public static String creditLine;
    private List<String> associationServicesDescriptionList;
    private List<String> associationServicesValueList;
    public static String currentInternetShoppingAbroadUseFlag = "N";
    public static String currentWithdrawCashFlag = "N";
    public static String currentOverdraftFlag = "N";
    private static String traceId;
    private static List<String> cuentas;
    private static int numeroCuentas;
    private static int numeroTarjeta;

    public static void findCreditCard(String customerId) {
        System.out.println("$Buscar si cuenta con tarjetas y jalar la data: " + customerId);
        String token = Security.createAccessToken();
        Response response = SerenityRest.given().header("Authorization", token)
                .header("Ocp-Apim-Subscription-Key", ServicesConfiguration.getProductsSubscriptionKey())
                .header("application", "ASSI")
                .header("company", "INTERBANK")
                .header("storeCode", "100")
                .header("user", "XT8052")
                .param("customerId", customerId.substring(6, 14))
                //.param("customerId", "0050003487")
                .get(ServicesConfiguration.getBaseUrl() + "/vision-productos/uat/cards");

        Security.deleteToken(token);
        LOGGER.info("[CONSULTA TARJETA] " + response.getBody().asString());
        cuentas = response.jsonPath().getList("accounts");
        numeroCuentas = response.jsonPath().getList("accounts").size();
        situationList = response.jsonPath().getList("accounts[0].cards.situation");
        statusList = response.jsonPath().getList("accounts[0].cards.status");
        cardNumber = response.jsonPath().getString("accounts[0].cards[0].cardNumber");
        creditLine = response.jsonPath().getString("accounts[0].creditLine");
        System.out.println(numeroCuentas);
        System.out.println(cuentas);
        System.out.println(situationList);
        System.out.println(statusList);
        System.out.println(cardNumber);
        System.out.println(creditLine);
    }

    public static Account getAccountAndCreditCard(String customerId) {
        System.out.println("$$Buscar si el cliente tiene una cuenta: " + customerId);
        String token = Security.createAccessToken();
        Response response = SerenityRest.given().header("Authorization", token)
                .header("Ocp-Apim-Subscription-Key", ServicesConfiguration.getProductsSubscriptionKey())
                .header("application", "ASSI")
                .header("company", "INTERBANK")
                .header("storeCode", "100")
                .header("user", "XT8052")
                .param("customerId", customerId.substring(6, 14))
                .get(ServicesConfiguration.getBaseUrl() + "/vision-productos/uat/cards");

        Security.deleteToken(token);
        LOGGER.info("[CONSULTA TARJETA] " + response.getBody().asString());
        Account account = new Account();
        account.setAccountNumber("0");
        if (response.getStatusCode() == 200) {
            numeroTarjeta = response.jsonPath().getList("accounts[0].cards").size();
            System.out.println(numeroTarjeta);
            account.setAccountNumber(response.jsonPath().getString("accounts[0].accountNumber"));
//            account.setCreditLine(response.jsonPath().getString("accounts[0].creditLine"));
//            for (int i = 0; i < numeroTarjeta; i++) {
//                account.addCreditCardData(new CreditCard(
//                        response.jsonPath().getString("accounts[0].cards[" + i + "].cardNumber"),
//                        response.jsonPath().getString("accounts[0].cards[" + i + "].mark"),
//                        response.jsonPath().getString("accounts[0].cards[" + i + "].status"),
//                        response.jsonPath().getString("accounts[0].cards[" + i + "].type"),
//                        response.jsonPath().getString("accounts[0].cards[" + i + "].beneficiary")
//                ));
//            }
        }
        return account;
    }

    public static List<Account>
    getCustomerAccount(String documentNumber, String type) {
        //System.out.println("Consulta servicio para obtener las CUENTAS del cliente: " + type + " " + documentNumber);
        Response response = SerenityRest.given().header("Authorization", Security.createAccessToken())
                .header("Ocp-Apim-Subscription-Key", ServicesConfiguration.getProductsSubscriptionKey())
                .header("application", "ASSI")
                .header("company", "INTERBANK")
                .header("storeCode", "100")
                .header("user", "XT8052")
                .param("identityDocumentType", type)
                .param("identityDocumentNumber", documentNumber)
                .get(ServicesConfiguration.getBaseUrl() + "/vision-productos/uat/account");

        LOGGER.info("[CONSULTA CUENTA] Response: {}", response.getBody().asString());
        List<Account> accounts = new ArrayList<>();
        if (response.statusCode() == 200) {
            //LOGGER.info("CUENTAS"+response.getBody().asString());

            numeroCuentas = response.jsonPath().getList("accounts").size();
           // List<Account> accounts = new ArrayList<>();
           // accounts=response.jsonPath().getList("accounts");
            for (int i = 0; i < numeroCuentas; i++) {
                accounts.add(new Account(
                        response.jsonPath().getString("accounts[" + i + "].type"),
                        response.jsonPath().getString("accounts[" + i + "].productCode"),
                        response.jsonPath().getString("accounts[" + i + "].subType"),
                        response.jsonPath().getString("accounts[" + i + "].subProductCode"),
                        response.jsonPath().getString("accounts[" + i + "].currency"),
                        response.jsonPath().getString("accounts[" + i + "].balance"),
                        response.jsonPath().getString("accounts[" + i + "].availableBalance"),
                        response.jsonPath().getString("accounts[" + i + "].status"),
                        response.jsonPath().getString("accounts[" + i + "].office"),
                        response.jsonPath().getString("accounts[" + i + "].accountNumber"),
                        response.jsonPath().getString("accounts[" + i + "].joint.type"),
                        response.jsonPath().getString("accounts[" + i + "].cci"),
                        response.jsonPath().getString("accounts[" + i + "].id")
                ));
            }
        }
        return accounts;
    }

    public static List<AccountCreditCard> getAccountFullData(ClientData clientData) {
        //System.out.println("Obtiene las TARJETAS del cliente: getUniqueCode " + clientData.getUniqueCode());
        String token = Security.createAccessToken();
        Response response = SerenityRest.given().header("Authorization", token)
                .header("Ocp-Apim-Subscription-Key", ServicesConfiguration.getProductsSubscriptionKey())
                .header("application", "ASSI")
                .header("company", "INTERBANK")
                .header("storeCode", "100")
                .header("user", "XT8052")
                .param("customerId", clientData.getUniqueCode().substring(6, 14))
                .get(ServicesConfiguration.getBaseUrl() + "/vision-productos/uat/cards");
        Security.deleteToken(token);
        LOGGER.info("[CONSULTA TARJETA] Response: {}", response.getBody().asString());
        List<AccountCreditCard> accountCreditCard = new ArrayList<>();
        if (response.getStatusCode() == 200) {
            numeroCuentas = response.jsonPath().getList("accounts").size(); //5 cuentas
            for (int h = 0; h < numeroCuentas; h++) {
                numeroTarjeta = response.jsonPath().getList("accounts["+h+"].cards").size();
                List<CreditCard> tarjetasPorCuenta = new ArrayList<>();
                for (int i = 0; i < numeroTarjeta; i++) {
                    tarjetasPorCuenta.add(new CreditCard(
                            response.jsonPath().getString("accounts[" + h + "].accountNumber"),
                            response.jsonPath().getString("accounts[" + h + "].cards[" + i + "].cardNumber"),
                            response.jsonPath().getString("accounts[" + h + "].cards[" + i + "].beneficiary"),
                            response.jsonPath().getString("accounts[" + h + "].cards[" + i + "].mark"),
                            response.jsonPath().getString("accounts[" + h + "].cards[" + i + "].situation"),
                            response.jsonPath().getString("accounts[" + h + "].cards[" + i + "].status"),
                            response.jsonPath().getString("accounts[" + h + "].cards[" + i + "].typeCode"),
                            response.jsonPath().getString("accounts[" + h + "].cards[" + i + "].type"),
                            response.jsonPath().getString("accounts[" + h + "].cards[" + i + "].activationDate")
                    ));
                }
                accountCreditCard.add(new AccountCreditCard(
                        response.jsonPath().getString("accounts[" + h + "].accountNumber"),
                        response.jsonPath().getString("accounts[" + h + "].creditLine"),
                        response.jsonPath().getString("accounts[" + h + "].currency"),
                        tarjetasPorCuenta
                ));

            }
        } else {
            clientData.setTypeClient("SIN_REGISTROS");
        }
        return accountCreditCard;
    }

    public static String verifyNewService(String codigoUnico) {
        System.out.println("()\\ Verificiar nuevo servicio: " + codigoUnico);
        String respuestaNewCliente = "NO_CLIENTE";
        Response response = SerenityRest.given()
                .header("processTraceId", "8c937db4-9c46-4427-82d1-de039b64892f")
                .header("X-INT-Service-Id", "APA")
                .header("X-INT-Supervisor-Id", "APPA0000")
                .header("X-INT-Branch-Id", "100")
                .header("X-INT-Country-Id", "PE")
                .header("Authorization", "Basic dUJzZUFzaUFBZG06SWJrYWRtMTgr")
                .header("X-INT-Device-Id", "13.10.13.1")
                .header("X-INT-Server-Id", "ASSICLOUD")
                .header("X-INT-Timestamp", "2022-04-20T16:27:37-05:00")
                .header("X-INT-Net-Id", "BI")
                .header("X-IBM-Client-Id", "9e48c834-31ba-4849-82e3-bada86634d22")
                .header("X-INT-CardId-Type", "0")
                .header("X-INT-Consumer-Id", "APP")
                .header("X-INT-Message-Id", "b8976809785638dad3dfbb8b1fc6a40f")
                .header("X-INT-User-Id", "APPA0000")
//                .param("queryType", "1")
                .get("https://10.11.32.73:7020/ibk/uat/api/customer/v3/" + codigoUnico + "/credit-cards?queryType=1");
        LOGGER.info("[CONSULTA CLIENTE] Response: {}", response.getBody().asString());
        System.out.println("[CONSULTA CLIENTE] Response: {}" + response.getBody().asString());
        if (response.getStatusCode() == 200) {
            respuestaNewCliente = response.jsonPath().getString("customer.personType");
            System.out.println(respuestaNewCliente);
        }
        return respuestaNewCliente;
    }

    public static String checkMovement(String cardId) {
        String estado = "ERROR";
        Configuration configuration = new Configuration();
        configuration.setearAmbiente("CONSULTA", "sspci", "uat", "bpi", "BPI");
        RestAssured.useRelaxedHTTPSValidation();
        String CONSULTAR_MOVIMIENTOS = "/templates/consultarMovimientosBody.json";
        Response response = SerenityRest.given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + Security.generoTokenBus(ApiCommons.SCOPE))
                .header("PCIauth", "Basic " + ApiCommons.PCI_AUTH)
                .header("traceId", "dba16e3cb3858278")
                .header("x-global-transaction-id", "20160623152105")
                .header("branchId", "898")
                .header("cardIdType", "0")
                .header("netId", "BI")
                .header("consumerId", "BPI")
                .header("messageId", "1660228135")
                .header("ipOrigen", "77.62.84.221")
                .header("supervisorId", "BPIB0000")
                .header("funcionalidadId", "mollit enim")
                .header("userId", "BPIB0000")
                .header("deviceId", "53.43.55.237")
                .header("serverId", "server12")
                .header("parentId", "22f5602d")
                .header("timeStamp", "2022-08-11T14:28:54.826Z")
                .header("referenceNumber", "REF06002")
                .header("countryCode", "PE")
                .header("groupMember", "08")
                .header("serviceId", ApiCommons.CANAL)
                .header("moduloId", "modcontacto")
                .header("channelId", ApiCommons.CANAL)
                .header("Ocp-Apim-Subscription-Key", ApiCommons.OPC_APIM_SUSCRIPTION_KEY)
                .header("Ocp-Apim-Subscription-Secret", ApiCommons.OPC_APIM_SUSCRIPTION_SECRECT)
                .header("Content-Type", "application/json")
                .body(ApiCommons.getTemplate(CONSULTAR_MOVIMIENTOS)
                        .replace("{cardId}", cardId))
                .post(ApiCommons.URL_BASE + "/en-credit-card/v1/movements/retrieve");
        if (response.getStatusCode() == 200) {
            if (response.getHeader("srvResponseMessage").equalsIgnoreCase("NO HAY DATA PARA LA CUENTA")) {
                estado = "NO";
            }
            if (response.getHeader("srvResponseMessage").contains("TRANSACCION EXITOSA")) {
                estado = "SI";
            }
        }
        return estado;
    }

    public static List<DebitCard> getDebitCard(String customerId) {
        Configuration configuration = new Configuration();
        configuration.setearAmbiente("CONSULTA", "sspci", "uat", "bpi", "APP");
        RestAssured.useRelaxedHTTPSValidation();
        Response response = SerenityRest.given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer dUJzZUFBREFBZG06YjFBQUoyWURL")
                .header("X-INT-Device-Id", "10.0.1.1")
                .header("X-INT-Timestamp", "2020-06-24T23:57:40")
                .header("X-INT-Service-Id", "BIP")
                .header("X-INT-Net-Id", "BI")
                .header("X-IBM-Client-Id", "87529776-e9cd-4ae3-abf0-1c8858ee2e1d")
                .header("X-INT-CardId-Type", "0")
                .header("X-INT-Supervisor-Id", "BIPE0000")
                .header("X-INT-User-Id", "BIPE0000")
                .get(ApiCommons.URL_BASE + "/ibk/uat/api/debit-card/v1/customer/"+customerId);
        List<DebitCard> debitCards = new ArrayList<>();
        if (response.getStatusCode() == 200) {
            LOGGER.info("[CONSULTA TARJETA DEBITO]:{}", response.getBody().asString());
            DebitCard debitCard = new DebitCard();
            debitCard.setUniqueCode(customerId);
            debitCard.setCardNumber(response.jsonPath().getString("cardId"));
            debitCard.setStatus_code(response.jsonPath().getString("status.code"));
            debitCard.setStatus_description(response.jsonPath().getString("status.description"));
            debitCard.setBlock_id(response.jsonPath().getString("block.id"));
            debitCards.add(getDebitCardFull(response.jsonPath().getString("cardId"),debitCard));
        }else{
            LOGGER.info("[CONSULTA TARJETA DEBITO]: No hay resultados para este codigo {}, Verifique su VPN",customerId);
        }
        //System.out.println("Hecho" + debitCards);
        return debitCards;
    }
    public static DebitCard getDebitCardFull(String cardNumber,DebitCard debitCard) {
        Configuration configuration = new Configuration();
        configuration.setearAmbiente("CONSULTA", "sspci", "uat", "bpifull", "APP");
        RestAssured.useRelaxedHTTPSValidation();
        Response response = SerenityRest.given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer dUJzZUFBREFBZG06YjFBQUoyWURL")
                .header("X-INT-Device-Id", "10.0.1.1")
                .header("X-INT-Timestamp", "2020-06-24T23:57:40")
                .header("X-INT-Service-Id", "BIP")
                .header("X-INT-Net-Id", "BI")
                .header("X-IBM-Client-Id", "87529776-e9cd-4ae3-abf0-1c8858ee2e1d")
                .header("X-INT-CardId-Type", "0")
                .header("X-INT-Supervisor-Id", "BIPE0000")
                .header("X-INT-User-Id", "BIPE0000")
                .get(ApiCommons.URL_BASE + "/ibk/uat/api/debit-card/v1/"+cardNumber);
        LOGGER.info("[CONSULTA DETALLE - TARJETA DEBITO] Response: {}", response.getBody().asString());
        if (response.getStatusCode() == 200) {
            debitCard.setCustomer_id(response.jsonPath().getString("customer.id"));
            debitCard.setCustomer_type(response.jsonPath().getString("customer.type"));
            debitCard.setBranchId(response.jsonPath().getString("branchId"));
            debitCard.setOpeningDate(response.jsonPath().getString("openingDate"));
            debitCard.setDueDate(response.jsonPath().getString("dueDate"));
            debitCard.setCardGroup(response.jsonPath().getString("cardGroup"));
            debitCard.setCardName(response.jsonPath().getString("cardName"));
            debitCard.setRequestId(response.jsonPath().getString("requestId"));
            debitCard.setStatus(response.jsonPath().getString("status"));
            debitCard.setEmbossingtype(response.jsonPath().getString("embossingtype"));
            debitCard.setCardSubType(response.jsonPath().getString("cardSubType"));
            debitCard.setCardTechType(response.jsonPath().getString("cardTechType"));
            debitCard.setCardType(response.jsonPath().getString("cardType"));
            debitCard.setLastReferenceNumber(response.jsonPath().getString("lastReferenceNumber"));
            debitCard.setPreviousReferenceNumber(response.jsonPath().getString("previousReferenceNumber"));
            debitCard.setLock_reasonId(response.jsonPath().getString("lock.reasonId"));
        }
        return debitCard;
    }
    public static ClientData getPerfilLevelClient(ClientData cd) {
        System.out.println("Buscar perfil" + cd.getDocumentClient());
        Configuration configuration = new Configuration();
        configuration.setearAmbiente("CONSULTA", "sspci", "uat", "bpi", "APP");
        RestAssured.useRelaxedHTTPSValidation();
        Response response = SerenityRest.given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + Security.generoTokenBus(ApiCommons.SCOPE))
                .header("traceId", "a5ab53ee660e1e5e")
                .header("x-global-transaction-id", "1")
                .header("branchId", "1")
                .header("cardIdType", "0")
                .header("netId", "BI")
                .header("consumerId", "APP")
                .header("messageId", "1660871652")
                .header("moduloId", "modcontacto")
                .header("ipOrigen", "216.255.180.171")
                .header("funcionalidadId", "1")
                .header("supervisorId", "S2058")
                .header("deviceId", "192.55.53.169")
                .header("serverId", "server12")
                .header("userId", "BIPE0001")
                .header("parentId", "6ece67d7")
                .header("timeStamp", "2022-01-28T18", "17", "02.356Z")
                .header("referenceNumber", "REF01092")
                .header("countryCode", "PE")
                .header("groupMember", "G0001")
                .header("serviceId", "APP")
                .header("channelId", "APP")
                .header("Ocp-Apim-Subscription-Key", ApiCommons.OPC_APIM_SUSCRIPTION_KEY)
                .header("Ocp-Apim-Subscription-Secret", ApiCommons.OPC_APIM_SUSCRIPTION_SECRECT)
                .header("PCIauth", "Basic " + ApiCommons.PCI_AUTH)
                .get(ApiCommons.URL_BASE + "/customer-access-entitlement/v1/"
                        +cd.getUniqueCode().substring(4)+"/digital-transactional-limit");
        System.out.println(response.getHeader("srvResponseMessage"));
        System.out.println(response.getHeader("busResponseMessage"));
        if (response.getStatusCode() == 200) {
            LOGGER.info("[CONSULTA CLIENTE] Response: {}", response.getBody().asString());
            cd.setProfileLevel(response.jsonPath().getString("'profileLevel'"));
            cd.setProfileLevelStatus(response.jsonPath().getString("'profileUploadIndicator'"));
            cd.setProfileLevelChange("N");
        }
        System.out.println(cd.getProfileLevel());
        System.out.println(cd.getProfileLevelStatus());
        return cd;
    }

    public static String getPerfilLevelClient(String cd) {
        Configuration configuration = new Configuration();
        configuration.setearAmbiente("CONSULTA", "sspci", "uat", "bpi", "APP");
        RestAssured.useRelaxedHTTPSValidation();
        Response response = SerenityRest.given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + Security.generoTokenBus(ApiCommons.SCOPE))
                .header("traceId", "a5ab53ee660e1e5e")
                .header("x-global-transaction-id", "1")
                .header("branchId", "1")
                .header("cardIdType", "0")
                .header("netId", "BI")
                .header("consumerId", "APP")
                .header("messageId", "1660871652")
                .header("moduloId", "modcontacto")
                .header("ipOrigen", "216.255.180.171")
                .header("funcionalidadId", "1")
                .header("supervisorId", "S2058")
                .header("deviceId", "192.55.53.169")
                .header("serverId", "server12")
                .header("userId", "BIPE0001")
                .header("parentId", "6ece67d7")
                .header("timeStamp", "2022-01-28T18", "17", "02.356Z")
                .header("referenceNumber", "REF01092")
                .header("countryCode", "PE")
                .header("groupMember", "G0001")
                .header("serviceId", "APP")
                .header("channelId", "APP")
                .header("Ocp-Apim-Subscription-Key", ApiCommons.OPC_APIM_SUSCRIPTION_KEY)
                .header("Ocp-Apim-Subscription-Secret", ApiCommons.OPC_APIM_SUSCRIPTION_SECRECT)
                .header("PCIauth", "Basic " + ApiCommons.PCI_AUTH)
                .get(ApiCommons.URL_BASE + "/customer-access-entitlement/v1/"
                        +cd.substring(4)+"/digital-transactional-limit");
//        System.out.println("[CONSULTA CLIENTE] Response: {}" + response.getBody().asString());
        System.out.println(response.getHeader("srvResponseMessage"));
        System.out.println(response.getHeader("busResponseMessage"));
        if (response.getStatusCode() == 200) {
            LOGGER.info("[CONSULTA CLIENTE] Response: {}", response.getBody().asString());
        }
        return cd;
    }
}

