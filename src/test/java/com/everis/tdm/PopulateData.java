package com.everis.tdm;

import com.everis.tdm.model.giru.Cliente;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PopulateData {

    @Test
    public void GenerateDataDesdeUnExcel() throws FileNotFoundException {
        Do.spLeerDocumentosExcel("CLIENTES", "TarjetaCredito");
    }

    @Test
    public void spGetClientByDocumentType() {
        Do.spGetClientByDocumentType("DNI").forEach(System.out::println);
    }

    @Test
    public void spGetTipoGestionByGestionPadre() {
        Do.spGetTipoGestionByGestionPadre("Garantías").forEach(System.out::println);
    }



    //@Test
    public void regsitroClientes() throws Exception {
      /*  Do.setNewCliente("DNI", "10217784", "0060529726", "",
                (""), (""), 0, "",
                (""), "Prueba/prueba.benni.everis");

        Do.setNewCliente("DNI", "10217786", "0060529727", "",
                (""), (""), 0, "",
                (""), "Prueba/prueba.benni.everis");*/
    }

    // @Test
    public void getCientes() {
        Do.getListClientes().forEach(System.out::println);
    }

    //@Test
    public void getCientesPorTarjetas() {
        Do.getListUsuariosPorTarjetas("1").forEach(System.out::println);
    }

    @Test
    public void getClienteTarjetas() throws InterruptedException {
        Do.getClienteTarjetaDebitoPorFiltroTdm("CUENTA CORRIENTE", "");
    }

    @Test
    public void tdmCrearClienteTarjeta() throws Exception {
        Do.setNewClienteTarjeta("", "", "++", "", "", "");
    }


    // @Test
    public void updateMillas() {
        Do.updateMillas("0060102593", 0);
    }

    //  @Test
    public void crearToken() throws Exception {
        String token = Do.crearToken2();
        Cliente cliente = Do.consultarDeuda(token);

        Do.clienteRegistroTDM(cliente.getNrodocumento(), cliente.getTipodocumento(), cliente.getTelefono(), cliente.getOperador(), cliente.getCantidadTarjetas());
        //manejamos el token
    }

    // @Test
    public void beeniProductos() {
     /*   Cliente cli = Do.getListClientes().stream().findFirst().get();

        System.out.println("ESTE CLIENTE ES :" + cli.getNumeroDocumento());
        System.out.println("SU TARJETA ES   :" + cli.getNumeroTarjeta());
        System.out.println("SUS MILLAS SON  :" + cli.getMillasTarjeta());

        //TODO CANJE DE PRODUCTOS EN BEENI
        // CANJEAR UN TOTAL DE PRODUCTOS POR 3000 MILLAS
        System.out.println("CLIENTE REALIZA EL CANJE DE PRODCUTOS CON 7000 MILLAS");

        Do.updateMillas(cli.getCodigoUnico(), cli.getMillasTarjeta() - 7000);
        System.out.println("EL CLIENTE AHORA TIENE " + Do.getListClientes().stream().findFirst().get().getMillasTarjeta()
                + " MILLAS EN TOTAL");*/
    }

    // @Test
    public void crearClienteConTarjetaGruadarDB() throws Exception {
        for (int i = 0; i < 10; i++)
            Do.crearClienteTarjeta();
    }

    @Test
    public void tdmCrearUsuario() throws Exception {
        Do.usuarioRegistroTDM("XT10436", "Area", "Callao2121++", "TEST");
    }

    @Test
    public void tdmLlamarUsuario() throws Exception {
        Do.getUsersPorRolYAmbiente("ESPECIALISTA", "UAT");
    }


    // @Test
    public void tdmActualizarUsuario() throws Exception {
        Do.updatePasswordUsuario("XT10436", "Callao2122++");
    }

    @Test
    public void tdmEliminarUsuario() throws Exception {
        Do.deleteUsuario("XT10436");
    }

    //@Test
    public void tdmCrearCliente() throws Exception {
        //LOGICA INTERNA PARA TRAER TARJTAS PENDIENTE
        Do.clienteRegistroTDM("DNI", "71401090", "Claro", "950276376", "1");
    }

    //@Test
    public void tdmCreatEscenario() throws Exception {
        // Do.crearEscenarioRegistroTDM(Numeroescenario,User,Password,Profile,TipoDocumento,NumeroDocumento,NumeroTarjeta,Tramite,Tipologia,Atencion,Perfil,AgregarComentarios,RespuestaCliente,AdjuntarArchivo,MedioRespuesta,NumeroCelular,NuevoRegistroRespuesta,RazonOperativa,TipoCMPMalConcretado,Operador,FechaConsumoMalConcretado,TipoRazonOperativa,MotivoSucedidoTarjeta,SeleccionarNoBloqueado,DescripcionNoBloqueado,NombreComercio,CantidadTarjetas);
    }


    // @Test
    public void filtroClientesNoRegistrados() {
        //    Stream<Cliente> optional = Do.getListClientes().stream().filter(x -> !x.isRegistroBenni());
        //   optional.forEach(System.out::println);
    }


    // @Test
    public void getClientesTdmParaCanjes() {
    /*    List<Cliente> list = Do.getListClientes().stream().filter(Cliente::isRegistroBenni).collect(Collectors.toList());
        Cliente opt = list.stream().filter(x -> x.getMillasTarjeta() >= 10000).findAny().orElse(list.stream().findAny().orElse(null));
        if (opt == null)
            System.out.println(" No existen clientes registrados con millas sufiente, se procede a realizar la recarga:");
            //TODO: Crear Cliente TDM + Millas
        else if (opt.getMillasTarjeta() < 10000)
            System.out.println("El cliente tiene pocas millas:");
        //TODO: AUMENTAR MILLAS POR LOYALTY
        System.out.println(opt);*/

    }

    //TODO: PRUEBA TIPOLOGIA
    @Test
    public void testGetTipologiaId() throws InterruptedException {
        Do.getTipologiaId("STG", "CUENTA", "SOLICITUD", "CANCELACIÓN DE CUENTA");
    }

    //TODO: PRUEBA MOTIVO
    @Test
    public void testGetMotivoId() throws InterruptedException {
        Do.getMotivoId("STG", "TARJETA DE CREDITO", "DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS");
    }

    //TODO: PRUEBA TIPOLOGIA COMERCIAL
    @Test
    public void testGetTipologiaIdComercial() throws InterruptedException {
        Do.getTipologiaIdComercial("UAT", "CUENTA CORRIENTE", "RECLAMO", "NO RECEPCIÓN DE DOCUMENTO");
    }

    //TODO: PRUEBA MOTIVO COMERCIAL
    @Test
    public void testGetMotivoIdComercial() throws InterruptedException {
        Do.getMotivoIdComercial("STG", "TARJETA DE CREDITO", "DUPLICADO DE ESTADO DE CUENTA/SERVICIO DE COPIAS");
    }

    //TODO: PRUEBA CONSULTA PRODUCTO
    @Test
    public void testConsultaProducto() throws InterruptedException {
        Do.getClienteTarjetaDebitoPorFiltroTdm("LINEA CONVENIO", "");
    }
    @Test
    public void getClientePorTipoDoc() throws InterruptedException {
        Do.getClientePorTipoDoc("01");
    }

    @Test
    public void testConsultaProductoDocumento() throws InterruptedException {
        Do.getClienteDocumentoProductoTdm("TARJETA DE CREDITO", "PASAPORTE");
    }
    //TODO: PRUEBA CONSULTA RESOLUCION RETAIL
    @Test
    public void obtenerDataEscenarioValidacion() throws Exception {
        System.out.println(Do.getEscenarioValidacion("CUENTA", "PEDIDO", "EXONERACION DE COBROS", ""));
    }

    //TODO: PRUEBA CONSULTA RESOLUCION COMERCIAL
    @Test
    public void obtenerDataResolucionComercial() throws Exception {
        System.out.println(Do.getResolucionComercial("CUENTA", "PEDIDO", "EXONERACION DE COBROS", ""));
    }

    //@Test
    public void CrearReclamo() throws InterruptedException {
        Do.PostRegistrarReclamoTdm("69788", "EXONERACIÓN DE COBROS", "2022/08/11 11:03:37", ".", ".", ".", "B17486", "Especialista", "DNI", "71401090", "4213 55** **** 1541", "1007003469143", "PEDIDO", "TARJETA DE DEBITO", "Si", "Si", "No", "Correo", "950276375", "Registrado", ".", ".", "BITEL", "", ".", ".", ".", ".", ".", ".");
    }

    // @Test
    public void clienteMillasTDM() throws InterruptedException {
        System.out.println(Do.clienteMillasTdm());
    }
    @Test
    public void test() throws InterruptedException {
        System.out.println(Do.getReclamoFiltros("Cliente","DNI","71401090"));
    }

    //@Test
    public void encriptarPassword() throws Exception {
        System.out.println(Do.setEncrypt("231400Aa."));


    }

}